# Code to aggregate aridity index - monthly into yearly totals and then get mean 
# From - 
# Test - 
# Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
import os, sys
import os.path
#from scipy import stats
#import eMAST.path_maker as pm
#import pandas as pd
#import eMAST.metadata as md
from numpy import genfromtxt
#import eMAST.netCDFTools as ncdf
from fnmatch import fnmatch

# Define function for reading ncfile
def read_ncdata(infile=None, varname=None):
    """ Returns data from netcdf file as outdata for use in other functions
    Requires an infile = filename and a varname = variable """
    g = Dataset(infile,'r')
    outdata = np.array(g.variables[varname])
    outdata = outdata[412:424,1783:1795]
    outdata = np.expand_dims(outdata,0)
    outdata[outdata == nodata] = decimal.Decimal("NaN")
    return outdata;

# Function to write an eMAST formatted netCDF file - example usage below...
def make_nc(outfile=None, varname=None, data=None, lati=None, loni=None, header=None, nodata=-9999.):
    ncds = Dataset(outfile, 'w', format='NETCDF4_CLASSIC', zlib=True)
    time  = ncds.createDimension('time', None)
    lat   = ncds.createDimension('latitude', len(lati))
    lon   = ncds.createDimension('longitude', len(loni))
    times = ncds.createVariable('time','f8',('time',)) 
    latitudes = ncds.createVariable('latitude','f4',('latitude',))
    longitudes = ncds.createVariable('longitude','f4',('longitude',))
    variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',), fill_value=nodata) 
    #variable = ncds.createVariable(varname,'f4',('latitude','longitude',), fill_value=nodata) 
    latitudes[:] = lati[:]
    longitudes[:] = loni[:]
    variable[:,:,:] = data # time version of the data insert 
    #variable[:,:] = data 
    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    # Removed time variable - add as required
    times.long_name = 'time' 
    times.standard_name = 'time' 
    times.units = 'year' 
    times.calendar = 'gregorian' 
    times.axis = 'T' 
    # Set geospatial global methods [basic method for setting global attr.]
    ncds.geospatial_lat_min = header['latsmin']
    ncds.geospatial_lat_max = header['latsmax']
    ncds.geospatial_lat_units = 'degrees_north'
    ncds.geospatial_lat_resolution = header['resolution']
    ncds.geospatial_lon_min = header['lonsmin']
    ncds.geospatial_lon_max = header['lonsmax']
    ncds.geospatial_lon_units = 'degrees_east'
    ncds.geospatial_lon_resolution = header['resolution']
    crs=ncds.createVariable('crs','f4')                                      
    # Set crs as variable
    crs.name="GDA94"
    crs.datum= "Geocentric_Datum_of_Australia_1994"
    crs.longitude_of_prime_meridian= 0.0
    crs.inverse_flattening=298.257222101
    crs.semi_major_axis=6378137.0
    crs.semi_minor_axis=6356752.314140356
    crs._CoordinateTransformType="Projection"
    crs._CoordinateAxisTypes="GeoX GeoY"
    # Set crs globally [alternative method!]
    setattr(ncds, 'crs:name', "GDA94")
    setattr(ncds, 'crs:datum', "Geocentric_Datum_of_Australia_1994")
    setattr(ncds, 'crs:longitude_of_prime_meridian', 0.0)
    setattr(ncds, 'crs:inverse_flattening', 298.257222101)
    setattr(ncds, 'crs:semi_major_axis', 6378137.0)
    setattr(ncds, 'crs:semi_minor_axis', 6356752.314140356)
    setattr(ncds, 'crs:_CoordinateTransformType', "Projection")
    setattr(ncds, 'crs:_CoordinateAxisTypes', "GeoX GeoY")
    # Close the file
    ncds.close()
    # Report back
    print 'Congratulations, your netCDF file is baked! See:', outfile

def makeYearArray(year):
    global itr
    itr = 0
    varName = 'layer'
    #varName = metaData.getStandardName()
    global fdata
    fdata = np.empty((1,11,11))
    for root, dirs, files in os.walk('.', topdown=True):
        for name in files:
            if fnmatch(name,'*_v1m1_'+str(year)+'??.nc'):
                if itr==0:
                    fdata = read_ncdata(name,varName)
                else:
                    new_data = read_ncdata(name,varName)
                    fdata = np.vstack((fdata,new_data))
                itr=itr+1
                print itr
                print('shape inside makeYearArray is: %s'% str(np.shape(fdata)))
                continue
    print itr
    fdata = np.mean(fdata,axis=0)
    fdata = np.expand_dims(fdata,0)
    print fdata
    print np.shape(fdata)
    return fdata;

def makeYearMean(data):
    mean=np.mean(data,axis=0)
    mean = np.expand_dims(mean,0)
    print('Making mean of: %s'%yearList)
    print(mean.shape)
    return mean;

a = Dataset('/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/alph/e_01/1970_2012/eMAST_R_Package_mon_alph_v1m1_201211.nc')
lat1 = np.array(a.variables['latitude'])[412:424]
lon1 = np.array(a.variables['longitude'])[1783:1795]

nodata = 9999.
os.chdir('/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/alph/e_01/1970_2012/')

startYear = 1970
endYear = 2013

yearList = range(startYear,endYear)

itry = 0
for year in yearList:
    if itry==0:
        yearArray = makeYearArray(year)
    else:
        newData = makeYearArray(year)
        yearArray = np.vstack((yearArray,newData))
    itry=itry+1
    print('shape at end of year for loop is: %s'%str(np.shape(yearArray)))
        
output = makeYearMean(yearArray)
print np.shape(output)

#output = np.squeeze(output,0)
print output

# Set ncfile header
x, y = (lon1, lat1) 
head = {'samples': len(x), \
  'lines': len(y), \
  'bands': 1, \
  'latsmin': min(lat1), \
  'lonsmin':min(lon1), \
  'latsmax': max(lat1), \
  'lonsmax':max(lon1), \
  'epsg':4326, \
  'resolution':0.01}

make_nc(outfile='/g/data/rr9/IM_PhD/data/aridity_annual_average_1970_2012_Litchfield.nc', 
                varname='aridity annual average 1970_2012', 
                data=output, 
                lati=lat1, 
                loni=lon1, 
                header=head)
