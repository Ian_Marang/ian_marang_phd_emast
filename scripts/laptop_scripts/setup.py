from distutils.core import setup

setup(
    name='pyeMAST',
    version='0.0.1',
    author='Dr Bradley J Evans',
    author_email='bradley.evans@mq.edu.au',
    packages=['tds', 'tds.test'],
    scripts=['bin/tds.py','bin/ozflux.py'],
    url='http://pyemast.emast.org.au/',
    license='LICENSE.txt',
    description='The Terrestrial Ecosystem Research Network ecological Modelling and Scaling Intrastructure (eMAST) python toolbox.',
    long_description=open('README.txt').read(),
    install_requires=[
        "pandas >= 0.13.1",
        "netCDF4 >= 1.0.8",
	"numpy >= 1.8.1",
	"scikit-learn >= 0.14"
    ],
