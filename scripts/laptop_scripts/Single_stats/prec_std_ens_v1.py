# Final working ensemble generator - standard deviation
# Uses hardcoded file location
# Gives std deviation data +/- 20 std deviation increments for each month

#Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
import os, sys
import os.path
from osgeo import gdal, osr

# Set nodata value
nodata = -9999.

# Define function for reading ncfile
def read_ncdata(infile=None, varname=None):
    """ Returns data from netcdf file as outdata for use in other functions
    Requires an infile = filename and a varname = variable """
    g = Dataset(infile,'r')
    outdata = np.array(g.variables[varname])
    outdata[outdata == nodata] = decimal.Decimal("NaN")
    return outdata;


# Get a list of the files to loop through
infile_list = ['/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197001.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197002.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197003.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197004.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197005.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197006.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197007.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197008.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197009.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197010.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197011.nc',
               '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197012.nc']

# Loop through files, append to array
for i, fn in enumerate(infile_list):
    if i==0:
        prec_data = read_ncdata(fn,'lwe_thickness_of_precipitation_amount')
    else:
        new_data = read_ncdata(fn,'lwe_thickness_of_precipitation_amount')
        prec_data = np.vstack((prec_data,new_data))
            
# Find standard deviation (ignoring NaN)
prec_std_inc = (np.nanstd(prec_data, axis=0))/20

# Generate products for each month + 20 standard deviation increments
for i in xrange(len(prec_data[:,0,0])):
    for k in range(1,21):
        perturb = prec_data[i,:,:]+(prec_std_inc*(k))
        vars()['prec_stddev_1970'+str(i).zfill(2)+'_ens'+str(k).zfill(2)] = perturb

# Generate products for each month - 20 standard deviation increments
for i in xrange(len(prec_data[:,0,0])):
    for k in range(1,21):
        perturb1 = (prec_data[i,:,:]+(prec_std_inc*-1*(k))).clip(0)
        vars()['prec_stddev_1970'+str(i).zfill(2)+'_ensn'+str(k).zfill(2)] = perturb1

# Get lats and lons from sample file
a = Dataset('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197001.nc')
lat1 = a.variables['latitude'][:]
lon1 = a.variables['longitude'][:]

# Function to write an eMAST formatted netCDF file - example usage below...

def make_nc(outfile=None, varname=None, data=None, lati=None, loni=None, header=None, nodata=-9999.):
    ncds = Dataset(outfile, 'w', format='NETCDF4_CLASSIC', zlib=True)
    time  = ncds.createDimension('time', None)
    lat   = ncds.createDimension('latitude', len(lati))
    lon   = ncds.createDimension('longitude', len(loni))
    #times = ncds.createVariable('time','f8',('time',)) 
    latitudes = ncds.createVariable('latitude','f4',('latitude',))
    longitudes = ncds.createVariable('longitude','f4',('longitude',))
    #variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',), fill_value=nodata) 
    variable = ncds.createVariable(varname,'f4',('latitude','longitude',), fill_value=nodata) 
    latitudes[:] = lati[:]
    longitudes[:] = loni[:]
    #variable[:,:,:] = data # time version of the data insert 
    variable[:,:] = data 
    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    # Removed time variable - add as required
    #times.long_name = 'time' 
    #times.standard_name = 'time' 
    #times.units = 'month' 
    #times.calendar = 'gregorian' 
    #times.axis = 'T' 
    # Set geospatial global methods [basic method for setting global attr.]
    ncds.geospatial_lat_min = header['latsmin']
    ncds.geospatial_lat_max = header['latsmax']
    ncds.geospatial_lat_units = 'degrees_north'
    ncds.geospatial_lat_resolution = header['resolution']
    ncds.geospatial_lon_min = header['lonsmin']
    ncds.geospatial_lon_max = header['lonsmax']
    ncds.geospatial_lon_units = 'degrees_east'
    ncds.geospatial_lon_resolution = header['resolution']
    crs=ncds.createVariable('crs','f4')                                      
    # Set crs as variable
    crs.name="GDA94"
    crs.datum= "Geocentric_Datum_of_Australia_1994"
    crs.longitude_of_prime_meridian= 0.0
    crs.inverse_flattening=298.257222101
    crs.semi_major_axis=6378137.0
    crs.semi_minor_axis=6356752.314140356
    crs._CoordinateTransformType="Projection"
    crs._CoordinateAxisTypes="GeoX GeoY"
    # Set crs globally [alternative method!]
    setattr(ncds, 'crs:name', "GDA94")
    setattr(ncds, 'crs:datum', "Geocentric_Datum_of_Australia_1994")
    setattr(ncds, 'crs:longitude_of_prime_meridian', 0.0)
    setattr(ncds, 'crs:inverse_flattening', 298.257222101)
    setattr(ncds, 'crs:semi_major_axis', 6378137.0)
    setattr(ncds, 'crs:semi_minor_axis', 6356752.314140356)
    setattr(ncds, 'crs:_CoordinateTransformType', "Projection")
    setattr(ncds, 'crs:_CoordinateAxisTypes', "GeoX GeoY")
    # Close the file
    ncds.close()
    # Report back
    print 'Congratulations, your netCDF file is baked! See:', outfile

# Set ncfile header
x, y = (lon1, lat1) 
head = {'samples': len(x), \
  'lines': len(y), \
  'bands': 1, \
  'latsmin': min(lat1), \
  'lonsmin':min(lon1), \
  'latsmax': max(lat1), \
  'lonsmax':max(lon1), \
  'epsg':4326, \
  'resolution':0.01}

# Run the make netCDF function for +pos stddev
for i in range(1,13):
    for j in range(1,21):
        make_nc(outfile='/g/data/rr9/IM_PhD/data/prec_ensemble/emast_prec_stddev_1970'+str(i).zfill(2)+'_ens'+str(j).zfill(2)+'.nc', 
                varname=str('prec_stddev_1970'+str(i).zfill(2)+'_ens'+str(j).zfill(2)), 
                data=vars()['prec_stddev_1970'+str(i).zfill(2)+'_ens'+str(j).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)

# Run the make netCDF function for -neg stddev
for i in range(1,13):
    for j in range(1,21):
        make_nc(outfile='/g/data/rr9/IM_PhD/data/prec_ensemble/emast_prec_stddev_1970'+str(i).zfill(2)+'_ensn'+str(j).zfill(2)+'.nc', 
                varname=str('prec_stddev_1970'+str(i).zfill(2)+'_ensn'+str(j).zfill(2)), 
                data=vars()['prec_stddev_1970'+str(i).zfill(2)+'_ensn'+str(j).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)