# Test Monthly anomaly generator with full metadata
# filepath for netCDFObj still processed manually in process func and makeMonthArray


#Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
import os, sys
import os.path
from scipy import stats
import eMAST.path_maker as pm
import pandas as pd
import eMAST.metadata as md
from numpy import genfromtxt
import eMAST.netCDFTools as ncdf
from fnmatch import fnmatch

# UPDATED - anomaly spelling, changed generateRandom function from 2 to original
def process(productsInputs,yearList,monthList):
	for productInput in productsInputs:
		print('Starting processing for %s') %(productInput)
		filePath = pm.FilePath(productInput)
		metaData = md.MetaData(productInput)
		
                #make array of month data 
		for month in monthList:
                    fdata = makeMonthArray(month,productInput)
     		    #open netcdf file
             	    netCDFLatsLons=""	
		    print('Getting longitude and latitude for all files')
             	    inputFile = '/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'+str(productInput[21:24])+'/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197001.nc'
		    netCDFObj = Dataset(inputFile,'r')
		    netCDFLatsLons=netCDFObj
                    totalStatistics ={}
	            totalStatistics['mean'] = makeMonthMean(fdata,month)
		    totalStatistics['standardDeviation']=makeMonthStandardDeviation(fdata,month)
		    totalStatistics['variance'] = makeMonthVariance(fdata,month)
		    totalStatistics['standardError'] = makeMonthStandardError(fdata,month)
		    saveTotalStatisticsNetCDF(totalStatistics,productInput,filePath,metaData,netCDFObj)
                    #run anom creation
                    fadata=makeMonthAnom(totalStatistics['mean'],totalStatistics['standardDeviation'],fdata,month,yearList,filePath, metaData,netCDFObj)
                    print('Finished loop '+str(month)+' , shape of fadata is '+str(fadata.shape))


def makeMonthArray(month,productInput):
    itr = 0
    os.chdir('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'+str(productInput[21:24])+'/e_01/1970_2012/')
    for root, dirs, files in os.walk('.', topdown=True):
        for name in files:
            if fnmatch(name,'*_v1m0_????'+str(month).zfill(2)+'.nc'):
                if itr==0:
                    fdata = read_ncdata(name,'lwe_thickness_of_precipitation_amount')
                else:
                    new_data = read_ncdata(name,'lwe_thickness_of_precipitation_amount')
                    fdata = np.vstack((fdata,new_data))
                itr=itr+1
                print(np.shape(fdata))
                continue
        return fdata

def makeMonthMean(data,month):
        mean=np.mean(data,axis=0)
        mean = np.expand_dims(mean,0)
        print('Making mean of: %s'%month)
        print np.mean(mean)
        return mean

def makeMonthStandardDeviation(data,month):
        std=np.nanstd(data,axis=0)
        std = np.expand_dims(std,0)
        print('Making std of: %s'%month)
        print np.mean(std)
        return std

def makeMonthVariance(data):
	# Find variance increment
	var = np.nanvar(data, ddof=1, axis=0)
	var = np.expand_dims(var,0)
	np.insert(var,0,0)
	print(var.shape)
	print(var)
	return(var)

def makeMonthStandardError(data):
	sem = stats.sem(data, axis=0)
	sem = np.expand_dims(sem,0)
	np.insert(sem,0,0)
	print(sem.shape)
	print(sem)
	return(sem)

def makeMonthAnom(mean,std, data, month,yearList,filePath,metaData,netCDFObj):
    	lat1 = np.array(netCDFObj.variables['latitude']) 
        lon1 = np.array(netCDFObj.variables['longitude'])
        varName = metaData.getStandardName()
        ensembleType = 'monthlyAnomalies'
	initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s_%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm,filePath.variable,ensembleType)
        for year in yearList:                        
    	    yearString= str(year)
            monthString=pm.fixZero(month)
            name = filePath.getInputDirectoryStructure(str(year),pm.fixZero(month),1)
            if fnmatch(name,'*_v1m0_????'+str(month).zfill(2)+'.nc'):
                print('Processing anomalies for: %s'%month)
                mean = np.squeeze(mean)
                std = np.squeeze(std)

                data=np.squeeze(read_ncdata(name,'lwe_thickness_of_precipitation_amount'))
                adata=(data-mean)/std
                adata=np.expand_dims(adata,0)
 
	        completeDirectoryPath='%s/%s/%s'%(initialDirectory,filePath.ensemble,filePath.timeSeries)
	        fileName = '%s_%s_%s_%s_%s_monthAnom_%s%s.nc'% (filePath.institution,
					filePath.model,filePath.frequency,filePath.variable,
					filePath.version,year,pm.fixZero(month))
	        completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
	        pm.checkAndCreatePath(completeDirectoryPath)

	        print('Writing Output for %s: ')%(completeFileName)
	        dateString = md.getDateStringFromFileName(fileName)
                dateTimeObj = md.convertStringToDateTime(dateString)
                gregorian = md.convertDateTimeToGregorian(dateTimeObj)
	        ncdf.make_nc(outfile=completeFileName,
	                     varname=varName,
	          	     data=adata,
	          	     time=gregorian,
	          	     metaData=None,
	          	     lati=lat1,
	          	     loni=lon1)
	print('Finished Writing Output for %s: ')%(completeFileName)

# UPDATED - to change variable name to standard
def saveTotalStatisticsNetCDF(totalStatistics,productInput,filePath,metaData,netCDFObj):
        
        lat1 = np.array(netCDFObj.variables['latitude'])
        lon1 = np.array(netCDFObj.variables['longitude'])
        varName = metaData.getStandardName()
	for totalStatisticKey,statisticArray in totalStatistics.iteritems():
		
		initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s_%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm,filePath.variable,totalStatisticKey)


        	completeDirectoryPath='%s/%s/%s'%(initialDirectory,filePath.ensemble,filePath.timeSeries)

        	fileName = '%s_%s_%s_%s_%s.nc'% (filePath.model,filePath.frequency,filePath.variable,totalStatisticKey,filePath.version)
        	completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
        	pm.checkAndCreatePath(completeDirectoryPath)

        	print('Writing Output for Total %s in %s: ')%(totalStatisticKey,completeFileName)
		#dateString = md.getDateStringFromFileName(fileName)        	
		#dateTimeObj = md.convertStringToDateTime(dateString)
		#gregorian = md.convertDateTimeToGregorian(dateTimeObj)
		ncdf.make_nc(outfile=completeFileName,
                	varname=varName,
                	data=statisticArray,
                	lati=lat1,
                	loni=lon1,
			metaData=None,
			time=None)
        	print('Finished Writing Output for Total %s in %s: ')%(totalStatisticKey,completeFileName)




# global variables
nodata=-9999.

# Establish which products to perturb

#productsInput=['eMAST_ANUClimate_mon_prec_v1m0_1970_2012']

#productsInput = ['eMAST_ANUClimate_mon_prec_v1m0_1970_2012',
# 		'eMAST_ANUClimate_mon_rrad_v1m0_1970_2012',
#		'eMAST_ANUClimate_mon_tmax_v1m0_1970_2012',
# 		'eMAST_ANUClimate_mon_tmin_v1m0_1970_2012',
# 		'eMAST_ANUClimate_mon_vapp_v1m0_1970_2012']

#productsOutput = ['random']
#productsOutput = ['anomalies','random']
#productsOutput = ['standardDeviation','variance','standardError','random']
#productsOutput = ['variance','standardError','random']
#productsOutput = ['standardError','random']



#establish the date range, .

startYear=1970
endYear=2013
yearList=range(startYear,endYear)
monthList = range(1,13)

# begin processing
process(productsInput,yearList,monthList)


#createRandomIndexCSV(80,0,10e-5)
#        mu, sigma = 0, 10e-5



