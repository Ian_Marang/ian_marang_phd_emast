import urllib2 as url
import xmlHandler as xml
class OAI_PMH:
	def __init__ (self,url):
		self.url = url
		self.metaDataQuery=None
		self.metaDataXML =None
		self.metaDataDataFrame=None
		self.metaDataDictionary=None
		
	def getMetaDataFormats(self):
		print('Getting meta data formats')
		listMethod = '?verb=ListMetadataFormats'
		
		self.metaDataQuery='%s%s' % (self.url,listMethod)
		response = url.urlopen(self.metaDataQuery)
		
		# create xml file from response object
		xmlObj1=xml.xmlObj(response.read())
		# make a list of metadata prefixes from xml file
		
		return xmlObj1
		
	def getMetaDataXML(self,fileIdentifier,metaDataFormat):
		print('Getting metadata from %s for %s') % (self.url,fileIdentifier)
                self.metaDataQuery='%s?verb=GetRecord&identifier=%s&metadataPrefix=%s' % (self.url, fileIdentifier,metaDataFormat)
                response = url.urlopen(self.metaDataQuery)

                # create xml file from response object
                xmlObj1=xml.xmlObj(response.read())
               
		return xmlObj1
		
		
		
	# probably cant do this as XML structure is not flat like data frame
	def convertXMLDataFrame(self):
		print('Converting XML to dataframe')


	
	def saveDataFrame(self):
		print('Saving metadata to dataframe locally')			


oia_pmh = OAI_PMH('http://rdsi-emast1-vm.intersect.org.au:8080/geonetwork/srv/eng/oaipmh')
#xmlObj = oia_pmh.getMetaDataFormats()
#print(xmlObj.xmlObj)
#print(xmlObj.xmlFileText)
#formatList = xmlObj.convertXMLToList('metadataPrefix')
#print(formatList)
metaData = oia_pmh.getMetaDataXML('6e42a84f-3079-4365-b0be-a18441083ea9','iso19139.anzlic')
print(metaData)
print(metaData.xmlFileText)
print(
# get xml file base n 


#print(formatList)




