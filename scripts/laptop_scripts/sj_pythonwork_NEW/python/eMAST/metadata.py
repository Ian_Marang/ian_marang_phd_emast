import path_maker as pm
import numpy as np
import pandas as pd
import netCDFTools as netcdf
import netCDF4
from netCDF4 import Dataset
from osgeo import gdal, osr
import re
import os
import datetime
source1 = os.path.join(os.path.dirname(__file__), 'csv/metadata.csv')
source2= os.path.join(os.path.dirname(__file__), 'csv/2015-06-12_metadata_input.csv')
metaDataSources=[source1,source2]

class MetaData:
        def __init__(self,variable):
                
		def determineRightMetaData(variable):
			print('lngth of sources %s') % (len(metaDataSources))
			rightMetaDataSource=""
			exceptCount=0
			for metaDataSource in metaDataSources:
				
                		# open data frame
				testMetaData= pd.DataFrame.from_csv(metaDataSource)			
				
				# try to find variable
				try:
					dataProductSeries= testMetaData.loc[variable, :]
					rightMetaDataSource = metaDataSource
				except:
					print('%s is not in %s') % (variable,metaDataSource)
					exceptCount = exceptCount+1
				if exceptCount==len(metaDataSources):
					rightMetaDataSource=None				

			print('The right metadata was located in file %s') %(rightMetaDataSource)
			return rightMetaDataSource

		
		self.metaDataSource=determineRightMetaData(variable)
                if self.metaDataSource!=None:	
			
			self.metaData= pd.DataFrame.from_csv(self.metaDataSource)
                	self.variable = variable
                	self.dataProductSeries= self.metaData.loc[variable, :]
                	self.variableDictionary=self.dataProductSeries.to_dict()
			print(self.dataProductSeries)
		
		
        
        def getLongName(self): 
                if self.metaDataSource!=None:
			print('Getting Long Name')
                	longName = self.variableDictionary['long_name'].decode('utf-8','ignore')
                	longName= longName.title()
		else: 
			longName = None
                return(longName)
        
        def getUnits(self):
                print('Getting Units')
                units = self.variableDictionary['units'].decode('utf-8','ignore')
                if units=='nounits' or units=='(no?units)':
                        units=""
   		if 'no' in units or'units' in units:
			units=""
		return(units)

	def getChartUnits(self):
		if self.metaDataSource !=None:
			print('Getting Units for the chars')
                	units = self.variableDictionary['units'].decode('utf-8','ignore')
                	if units=='nounits' or units=='no?units' or units=='no_unit':
                        	units=""
			else:
				units = '(%s)'%(units)
                else:
			units=None
		return(units)

	def getStandardName(self):
		print('Getting Standard Namee')
		standardName = self.variableDictionary['standard_name'].decode('utf-8','ignore')
		return(standardName)

	def writeMetaData(self):
		
		# find the file path of the based on the variable name
		directory = pm.namingConventionStagingDirectory(self.variable)
		
		# final directory
		finalDirectory = '%s/final/' % (directory)
		print('Directory containing files to write meta data %s') %(directory)		

		# get a list of all the directories in path
		files = pm.getFileNameList(directory)
		metaData = self.dataProductSeries
	        	
		# loop through the files in the path, make complete path, 
		for file in files:
			if file.endswith(".nc"):
				completePath = '%s%s'%(directory,file)
				print(file)
				#import sys
				#sys.exit()
				print('Updating meta data for %s: ')%(completePath)
				#get string date from netcdfFileName
				dateString= getDateStringFromFileName(file)
				print(dateString)

				dateTimeObj = convertStringToDateTime(dateString)
				print(dateTimeObj)

				gregorian = convertDateTimeToGregorian(dateTimeObj)
				print(gregorian)
				
				
		
				#open existing netcdf file
				ncdfObj = netcdf.ncopen(completePath)
					
				# get longitude
				lon = netcdf.getLongitudeArray(completePath)
				# get latitude
				lat = netcdf.getLatitudeArray(completePath)
				finalPath = '%s%s' %(finalDirectory,file)	
				# get data
				data = netcdf.getVariableArray(completePath,'layer')
				
					
				#create new file staging directory for new file
				pm.checkAndCreateDirectory(finalDirectory)	
				
				
				netcdf.make_nc(outfile=finalPath,
						varname=self.getStandardName(),
						data= data,
						lati=lat,
						loni = lon,
						metaData=metaData,
						time=gregorian)
				print(netcdf)
				
			

		


def getDateStringFromFileName(fileName):
	print('Getting date string from file name %s') %fileName
	pattern = '\d{6}'
	searchObject=re.search(pattern,fileName)	
	return(searchObject.group())
	

def convertStringToDateTime(dateString):
	print('Converting date string %s to dateTime object') %dateString
	# obtain year from string and convert to integer
	year = int(dateString[0:4])
	
	#obtain month from string and convert to integer
	month= int(dateString[4:6])

	# default day as 1
	day = 1

	#create datetime object
	dateTimeObj = datetime.date(year, month, day)
	return dateTimeObj

	
def convertDateTimeToGregorian(dateTimeObject):
	print('Converting date time object to gregorian')
	gregorian = dateTimeObject.toordinal()
	return gregorian

