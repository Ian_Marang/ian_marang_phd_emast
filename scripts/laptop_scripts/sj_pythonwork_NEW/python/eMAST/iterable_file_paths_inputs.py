import path_maker as pm


class Iterable_File_Paths_Inputs:

	def __init__(self,variable,yearStart,yearEnd):
		self.variable = variable
		self.yearStart = yearStart
		self.yearEnd = yearEnd
		self.dates = pm.getYearMonthDayArray(yearStart,yearEnd)
		self.completeFilePaths =[]
		filePathMaker = pm.FilePath(variable)
		for date in self.dates:
			completeFilePath = filePathMaker.getInputDirectoryStructure(date['year'],date['month'],date['day'])
			self.completeFilePaths.append(completeFilePath)

 		
