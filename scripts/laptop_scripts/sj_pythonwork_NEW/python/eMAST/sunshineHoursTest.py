import netCDFTools as ncdf
import math
import path_maker as pm
import numpy as numpy
class SunshineMonthlyConverter:
	def __init__(self, input):
		self.startYear=input['startYear']
		self.endYear=input['endYear']
		self.varName=input['varName']
		self.inputDirectoryPath=input['inputDirectoryPath']
		self.inputFileNameBase=input['inputFileNameBase']
		self.outputDirectoryPath=input['outputDirectoryPath']
		self.outputFileNameBase=input['outputFileNameBase']

	def convert(self):
		years=range(self.startYear,self.endYear+1)
		months=range(1,13)
		for year in years:
			for month in months:
				if month <10:
					month='0%s' % (str(month))
				else:
					month=str(month)
				inputPath = '%s/%s%s%s.nc'%(self.inputDirectoryPath,self.inputFileNameBase,year,month)
				print('input path %s') %(inputPath)
			
				# open net cdf file
				print('Opening net cdf file %s') % (inputPath)
				
				print('Getting data for variable %s') % (self.varName)
				data = ncdf.getVariableArray(inputPath,self.varName)
				print(data)
				print(data.shape)
				# get longitude
				longitude = ncdf.getLongitudeArray(inputPath)
				print(longitude)
				print(longitude.shape)
				
				# get latitude		
				latitude = ncdf.getLatitudeArray(inputPath)
				print(latitude)
				print(latitude.shape)

				# send to sunshine hour function			
				data = sunshineHours(int(month),data,latitude)
	
				# create outfile Name
				outputPath = '%s%s%s%s.nc'%(self.outputDirectoryPath,self.outputFileNameBase,year,month)
				print(self.outputDirectoryPath) 				
				pm.checkAndCreateDirectory(self.outputDirectoryPath)
						
				# save net cdf
				ncdf.make_nc(outputPath,'sunshineHoursTest',data,latitude,longitude,None)


def sunshineHours(month,radArray,latArray): 
         
	mdays=[31,28,31,30,31,30,31,31,30,31,30,31] 
	mrsun=numpy.zeros(shape=(360,720)) 
#       for month in months: 
                #print(months) 
                #import sys 
                #sys.exit('exit') 
        print ('Month %s') %(month) 
        mrtoa = numpy.zeros(shape=(360,720)) 
        days = mdays[month-1] 
        dayRange = range(1,days+1) 
        for day in dayRange:     
                print ('Month %s Day %s') %(month,day) 
                print('Converting to sunshine hours') 
                #newArray = empty(shape=radArray.shape)*numpy.nan 
                rtoa = cf_TRA(day,radArray,latArray) 
                print('rtoa1') 
                print(rtoa) 
                rtoa = numpy.repeat(rtoa[:,numpy.newaxis],radArray.shape[1],1) 
                print('rtoa2') 
                print(rtoa) 
                mrtoa +=(rtoa /mdays[month-1]) 
                print('mrtoa')   
                print[mrtoa.shape] 
                print(mrtoa)             
        print(radArray) 
        mrsun = ((radArray/mrtoa)-0.25 )/0.5 
        #mrsun[numpy.is_nan()]=0 
        print('mrsun') 
        print(mrsun) 
        
        return mrsun 
    
def cf_TRA(day,radArray,latArray): 
      
# Calculates the total radiation at the top of the atmosphere based on 
# latitude and day of the year. 
     print('Calculatinig total radiation at top of the atmosphere') 
      
     print('Calculating msolc') 
         
     msolc= cf_SOLC(day) 
     print(msolc)  
     print('Calculating DELT') 
    
     delta= cf_DELT(day) 
     print(type(delta)) 
     print(delta)       

     print('claculating x,y')   
     x,y = cf_XYproj(latArray,delta) 
     
     print('x') 
     print(x) 
     print('y') 
     print(y)  
     hs= cf_HS(x,y,latArray, delta) 
     rtoa_MJ = cf_RTA(msolc,hs,x,y)*nsecs*1e-6 
     return rtoa_MJ 


def cf_SOLC(day): 
# calculates the solar constant based on day 
    arg = cf_ARG(day) 
    o_solc = solc*(1.+2.*eccen*math.cos(arg*d2r)) 
    return o_solc 

def cf_ARG(day): 
     arg = 360*day/365*d2r 
     return arg 

def cf_DELT(day): 
# calculates the change in solar angle (solar declination) 
    arg = cf_ARG(day+10) 
    o_delta = -23.4*math.cos(arg*d2r) 
    return (o_delta) 


def cf_HS(x,y,latArray,delta): 
    # if values of x are greater than y, set HS to  pi 
    # if values of x are < -y set to 0   
    Hs = numpy.empty(shape=latArray.shape)*numpy.nan 
    Hs = numpy.arccos(-1.*numpy.tan(latArray*d2r)*numpy.tan(delta*d2r)) 
    Hs[x>y]=math.pi 
    Hs[x<-1*y]=0 
    print(Hs.shape) 
    print(Hs) 
    return Hs


def cf_XYproj(latArray,delta): 
# solar coordinate projections 
    x = numpy.sin(latArray*d2r)*numpy.sin(delta*d2r) 
    y = numpy.cos(latArray*d2r)*numpy.cos(delta*d2r) 
    return x,y 


def cf_RTA(msolc, hs, x, y): 
# calculates PAR at the top of the atmosphere (W m-2) 
    rta = msolc*(hs*x+numpy.sin(hs)*y)/math.pi; 
    return rta 



################################################################################
# run from here


# global constants
eccen = 0.01675
d2r= math.pi/180.0
cw= 1.0
solc= 1360.8
dtime = 0.0036
nsecs= 86400.0
albedo = 0.17
    

# ewatchData
eWATCH = {'startYear':1980,
	  'endYear':1981,
	  'varName':'eMAST_eWATCH_mon_srad_v1m0_1979_2012',
          'inputDirectoryPath':'/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eWATCH/0_5deg/v1m1/mon/land/srad/e_01/1979_2012/',
          'inputFileNameBase':'eWATCH_mon_srad_v1m1_', 
          'outputDirectoryPath':'/g/data/rr9/sjj576/sunshineHoursTest/',
          'outputFileNameBase':'eWATCH_mon_suns_v1m1_'}

# need to create a new dictionary for the belgian data once it is in monthly a monthly format
"""
belgian = {'startYear':1980,
          'endYear':1981,
          'varName':'',
          'inputDirectoryPath':'',
          'inputFileNameBase':'',
          'outputDirectoryPath':'',
          'outputFileNameBase':''}
"""



# create converter object
converter = SunshineMonthlyConverter(eWATCH)

# run converter
converter.convert()




