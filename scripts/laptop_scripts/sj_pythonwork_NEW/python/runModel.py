import eMAST.netCDFTools as ncdf
import numpy as np
import eMAST.splash as sp
import eMAST.path_maker as pm
import model_engine as me
import datetime
import netCDF4
from netCDF4 import Dataset
import eMAST.model_inputs as mi
import datetime
fmt = '%Y.%m.%d'

modelInputs = mi.Model_Inputs(['eMAST_eWATCH_day_srad_v1m1_1979_2012','eMAST_eWATCH_day_prec_v1m1_1979_2012','eMAST_eWATCH_day_tair_v1m1_1979_2012'],['/home/576/sjj576/bitbucket/eMAST/python/eMAST/netCDF/WFDEI-elevation.nc'],1981,1982)

print modelInputs.iterableModelInput[0].completeFilePaths
print modelInputs.constantPathList[0]

model = me.model_engine(modelInputs,1981,1982)

#elevation
elevationPath= '/home/576/sjj576/bitbucket/eMAST/python/eMAST/netCDF/WFDEI-elevation.nc'

# get array of string of all through years,months and days
dates=pm.getYearMonthDayArray(1981,1982)

print(dates)
pm.createStagingDirectory('eMAST_eWATCH_day_eteq_v1m1_1979_2012')
pm.createStagingDirectory('eMAST_eWATCH_day_etpo_v1m1_1979_2012')
pm.createStagingDirectory('eMAST_eWATCH_day_etac_v1m1_1979_2012')


# inputs
#create filePaths objects
for date in dates:
	filePathPrec = pm.FilePath('eMAST_eWATCH_day_prec_v1m1_1979_2012')
	filePathTair = pm.FilePath('eMAST_eWATCH_day_tair_v1m1_1979_2012')
	filePathSrad = pm.FilePath('eMAST_eWATCH_day_srad_v1m1_1979_2012')
	
	filePathInputPrec = filePathPrec.getInputDirectoryStructure(date['year'],date['month'],date['day'])
	filePathInputTair = filePathTair.getInputDirectoryStructure(date['year'],date['month'],date['day'])
	filePathInputSrad = filePathSrad.getInputDirectoryStructure(date['year'],date['month'],date['day'])

	precNetCDF = ncdf.ncopen(filePathInputPrec)
	precArray = np.array(precNetCDF.variables['convective_precipitation_flux'])
	precNetCDF.close()
	precArray=precArray[0]
	print precArray
	print(precArray.shape)

	tairNetCDF = ncdf.ncopen(filePathInputTair)
	tairArray = np.array(tairNetCDF.variables['air_temperture'])
	tairNetCDF.close()
	tairArray=tairArray[0]
	print tairArray
	print(tairArray.shape)


	sradNetCDF = ncdf.ncopen(filePathInputSrad)
	sradArray = np.array(sradNetCDF.variables['surface_downwelling_shortwave_flux_in_air'])
	sradArray=sradArray[0]
	lats=np.array(sradNetCDF.variables['latitude'])
        lons=np.array(sradNetCDF.variables['longitude'])

	sradArray = sp.sunshineHours(sradArray,lats)
	print sradArray
	print(sradArray.shape)
	
	sradNetCDF.close()
	# need to convert to sunshine hours
	
	s = '%s.%s.%s'% (str(date['year']),pm.fixZero(date['month']),pm.fixZero(date['day']))
	
	
	elevationNetCDF = ncdf.ncopen(elevationPath)
	elevationArray = np.array(elevationNetCDF.variables['elevation'])
	elevationNetCDF.close()
	print elevationArray
	print(elevationArray.shape)

	dt = datetime.datetime.strptime(s, fmt)
	tt = dt.timetuple()
	dayOfYear=(tt.tm_yday)
	print(dayOfYear)

	evap_g =sp.EVAP_G(dayOfYear,elevationArray,sradArray,tairArray,precArray,date['year'])
	
	
	#daily cond
	print(evap_g.w)
        w = evap_g.w
        filePathEteq = pm.FilePath('eMAST_eWATCH_day_cond_v1m1_1979_2012')
        outputDirectoryCond = filePathEteq.getOutputDirectoryStructure()
        outputFileNameCond = filePathEteq.getOutputFileName(date['year'],date['month'],date['day'])+'.nc'
        completePathEteq = '%s%s'%(outputDirectoryCond,outputFileNameCond)
        print(outputDirectoryEteq)

        ncdf.make_netCDF4CF16_daily  (data=eet, year=date['year'],month=date['month'], \
                                             day=None,  \
                                             metadata=None, \
                                             outdirectory=outputDirectoryCond, \
                                             outfilename=outputFileNameCond, \
                                             header=ncdf.getHeader(lons,lats), \
                                             lats=lats, \
                                             lons=lons, \
					     productname='eMAST_eWATCH_day_cond_v1m1_1979_2012')




	
	# daily EET
	print(evap_g.eet_d)
	eet = evap_g.eet_d
	filePathEteq = pm.FilePath('eMAST_eWATCH_day_eteq_v1m1_1979_2012')
	outputDirectoryEteq = filePathEteq.getOutputDirectoryStructure()
	outputFileNameEteq = filePathEteq.getOutputFileName(date['year'],date['month'],date['day'])+'.nc'
	completePathEteq = '%s%s'%(outputDirectoryEteq,outputFileNameEteq)
	print(outputDirectoryEteq)
	
	ncdf.make_netCDF4CF16_daily  (data=eet, year=date['year'],month=date['month'], \
                                             day=None,  \
                                             metadata=None, \
                                             outdirectory=outputDirectoryEteq, \
                                             outfilename=outputFileNameEteq, \
                                             header=ncdf.getHeader(lons,lats), \
                                             lats=lats, \
                                             lons=lons, \
                                             productname='eMAST_eWATCH_day_eteq_v1m1_1979_2012')

	# daily PET
	print(evap_g.pet_d)
	pet = evap_g.pet_d
	filePathEtpo = pm.FilePath('eMAST_eWATCH_day_etpo_v1m1_1979_2012')
        outputDirectoryEtpo = filePathEtpo.getOutputDirectoryStructure()
        outputFileNameEtpo = filePathEtpo.getOutputFileName(date['year'],date['month'],date['day'])+'.nc'

        completePathEtpo = '%s%s'%(outputDirectoryEtpo,outputFileNameEtpo)
        print(outputDirectoryEtpo)
        ncdf.make_netCDF4CF16_daily  (data=pet, year=date['year'],month=date['month'], \
                                             day=None,  \
                                             metadata=None, \
                                             outdirectory=outputDirectoryEtpo, \
                                             outfilename=outputFileNameEtpo, \
                                             header=ncdf.getHeader(lons,lats), \
                                             lats=lats, \
                                             lons=lons, \
                                             productname='eMAST_eWATCH_day_etpo_v1m1_1979_2012')

	

	
	# daily AET
	print(evap_g.aet_d)
	aet = evap_g.aet_d
	filePathEtac = pm.FilePath('eMAST_eWATCH_day_etac_v1m1_1979_2012')
        outputDirectoryEtac = filePathEtac.getOutputDirectoryStructure()
        outputFileNameEtac = filePathEtac.getOutputFileName(date['year'],date['month'],date['day'])+'.nc'

        completePathEtac = '%s%s'%(outputDirectoryEtac,outputFileNameEtac)

        ncdf.make_netCDF4CF16_daily  (data=aet, year=date['year'],month=date['month'], \
                                             day=None,  \
                                             metadata=None, \
                                             outdirectory=outputDirectoryEtac, \
                                             outfilename=outputFileNameEtac, \
                                             header=ncdf.getHeader(lons,lats), \
                                             lats=lats, \
                                             lons=lons, \
                                             productname='eMAST_eWATCH_day_etac_v1m1_1979_2012')

	
	#precipitation
#precPath='/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eWATCH/0_5deg/v1m1/day/land/prec/e_01/1979_2012/eWATCH_day_prec_v1m1_19790101.nc'
#tair
#tairPath='/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eWATCH/0_5deg/v1m1/day/land/tair/e_01/1979_2012/eWATCH_day_tair_v1m1_19790101.nc'

# radiation
#sradPath='/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eWATCH/0_5deg/v1m1/day/land/srad/e_01/1979_2012/eWATCH_day_srad_v1m1_19910101.nc'

# elevation
#elevationPath= '/home/576/sjj576/bitbucket/eMAST/python/eMAST/netCDF/WFDEI-elevation.nc'

#date of the year 19710101
#surface_downwelling_shortwave_flux_in_air', \
#              'air_temperture', \
#              'convective_precipitation_flux
#eMAST_eWATCH_mon_prec_v1m0_1979_2012

#print(evap_g)

# daily condensation

#print(evap_g.wc)

# daily EET
#print(evap_g.eet_d)


# daily PET
#print(evap_g.pet_d)

# daily AET
#print(evap_g.aet_d)

	
	

#precipitation
#precPath='/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eWATCH/0_5deg/v1m1/day/land/prec/e_01/1979_2012/eWATCH_day_prec_v1m1_19790101.nc'
#tair
#tairPath='/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eWATCH/0_5deg/v1m1/day/land/tair/e_01/1979_2012/eWATCH_day_tair_v1m1_19790101.nc'

# radiation
#sradPath='/g/data/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/eWATCH/0_5deg/v1m1/day/land/srad/e_01/1979_2012/eWATCH_day_srad_v1m1_19910101.nc'

# elevation
#elevationPath= '/home/576/sjj576/bitbucket/eMAST/python/eMAST/netCDF/WFDEI-elevation.nc'

#date of the year 19710101
#surface_downwelling_shortwave_flux_in_air', \
#              'air_temperture', \
#              'convective_precipitation_flux
#eMAST_eWATCH_mon_prec_v1m0_1979_2012




#precNetCDF = ncdf.ncopen(precPath)
#precArray = np.array(precNetCDF.variables['convective_precipitation_flux'])
#precArray=precArray[0]
#print precArray
#print(precArray.shape)

#tairNetCDF = ncdf.ncopen(tairPath)
#tairArray = np.array(tairNetCDF.variables['air_temperture'])
#tairArray=tairArray[0]
#print tairArray
#print(tairArray.shape)


#sradNetCDF = ncdf.ncopen(sradPath)
#sradArray = np.array(sradNetCDF.variables['surface_downwelling_shortwave_flux_in_air'])
#sradArray=sradArray[0]
#print sradArray
#print(sradArray.shape)
# need to convert to sunshine hours


#elevationNetCDF = ncdf.ncopen(elevationPath)
#elevationArray
#elevationArray = np.array(elevationNetCDF.variables['elevation'])
#print elevationArray
#print(elevationArray.shape)

#evap_g =sp.EVAP_G(1,elevationArray,sradArray,tairArray,precArray,1970)
#print(evap_g)

# daily condensation

#print(evap_g.wc)

# daily EET
#print(evap_g.eet_d)


# daily PET
#print(evap_g.pet_d)

# daily AET
#print(evap_g.aet_d)









