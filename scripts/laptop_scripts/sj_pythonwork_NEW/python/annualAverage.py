import numpy as np
import eMAST.path_maker as pm
import eMAST.metadata as md
import sys
import eMAST.netCDFTools as ncdf
import netCDF4
from netCDF4 import Dataset

class Converter:
        def __init__(self,startYear,endYear,inputVarName,outputVarName):
                self.startYear=startYear
                self.endYear=endYear
                self.inputVarName=inputVarName
                self.outputVarName=outputVarName
		self.inputFilePath =pm.FilePath(self.inputVarName)
		self.outputFilePath=pm.FilePath(self.outputVarName)
             
                self.variableArray=None
                self.latArray=None
                self.longArray=None
                self.variable3d=None
		self.inputMetaData=md.MetaData(self.inputVarName)
		self.outputMetaData=md.MetaData(self.outputVarName)
		 
        def convertMonthlyToAnnual(self,aggregateType):

                # set up years /months to process
                years = range(self.startYear,self.endYear)
                if self.inputFilePath.frequency=='mon':
                        monthsToProcess=12

             
                months = range(1,monthsToProcess+1)
                day =1

                #iterate though each year sequentially
		for year in years:
                        #iterate though each year sequentially
			for month in months:
                                print(month)
                                                          
				# get input path
				inputFilePath=self.inputFilePath.getInputDirectoryStructure(year,month,day)
				print('Input Path: '+inputFilePath)

				#get output directory and fileanme
				outputDirectoryStructure=self.outputFilePath.getOutputDirectoryStructure()
				outputFileName = self.outputFilePath.getOutputFileName(year,month,day)
				print('Output directory Structure: '+outputDirectoryStructure)
				print('Output file name: '+outputFileName)
				outputPath = '%s%s' % (outputDirectoryStructure,outputFileName)
				#open netcdf file from input directory
				netCDFObj = Dataset(inputFilePath)
				print(netCDFObj)

				# get variable name of input data
				
				
				# get data, lat and long from netcdfFile
				self.variableArray = netCDFObj.variables['layer'][:]
				self.latArray = netCDFObj.variables['latitude'][:]
				self.lonArray =  netCDFObj.variables['longitude'][:]

				#close net cdf
				netCDFObj.close()
	
				print('Obtaining month %s') % (month)
				if month == 1:
					self.variable3d = self.variableArray
					print(self.variable3d.shape)
					#sys.exit()
				if month>1:
					print('Stacking %s') % (year)
					self.variable3d = np.dstack((self.variable3d, self.variableArray))
					print(self.variable3d)
					print(self.variable3d.shape)
				if month ==12:
					if aggregateType == 'mean':
						self.variableArray = np.mean(self.variable3d,axis=2)
					if aggregateType == 'sum':
                                                self.variableArray = np.sum(self.variable3d,axis=2)
					print(self.variableArray)
					print(self.variableArray.shape)
				
				# get standard name from metaData
				standardName = self.outputMetaData.getStandardName()
				
				#get date string
				dateString = ('%s01') % (str(year),)
				# convert year to gregorian
				dateTimeObject = md.convertStringToDateTime(dateString)
				gregorianDate = md.convertDateTimeToGregorian(dateTimeObject)

					
			# write netcdf file after months fininshed looping
			ncdf.make_nc(outputPath,standardName,self.variableArray,self.latArray,self.lonArray,self.outputMetaData,gregorianDate)


# create converter object
converter = Converter(1979,2013,'eMAST_eWATCH_mon_alph_v1m1_1979_2012','eMAST_eWATCH_mon_alph_v1m1_1979_2012')


# convert date into annual
converter.convertMonthlyToAnnual('mean')
