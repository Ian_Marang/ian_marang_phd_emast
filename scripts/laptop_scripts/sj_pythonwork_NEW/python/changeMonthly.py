####################################################################################
# Date : Thursday 27 November 2014
# Description : Script for reading the watch data and converting it to netCDF CF
# Authors : Bradley Evans
# License : CC BY Version 4.0 (international licence)
# http://creativecommons.org/licenses/by/4.0/legalcode 
# Log :
# 2014-11-27 : Assigned licencse, adapted to use in the ANDS MODC Project
####################################################################################
####################################################################################
# 
# Functions for generating netCDF CF files
# Description:
# Coded as a main function, controlled below
####################################################################################
# Load libraries
import os
import datetime
import pandas
import argparse
import fnmatch
import numpy
import pandas
from osgeo import gdal, osr
import netCDF4
import numpy as np
import os
try: from collections import OrderedDict
except ImportError:
    try: from ordereddict import OrderedDict
    except ImportError:
        print "Require OrderedDict, https://pypi.python.org/pypi/ordereddict"
        raise
from scipy.io import netcdf
####################################################################################
# NETCDF_BUILDER.py
#
# Author:
# Matt Paget, CSIRO Marine and Atmospheric Research, Canberra, Australia.
# Initial testing and development was provided by Edward King and Ziyuan Wang.
# The development of this software was supported by the TERN/AusCover project.
# The software is open source and is released under Creative Commons
# Attribution (CC-BY).  Comments, questions, revisions and recommendations
# can be directed to <data at auscover.org.au>.
#
# Version: 3.3   12 October 2013
# Requires:
#  NumPy
#  OrderedDict
#  https://code.google.com/p/netcdf4-python/
####################################################################################
"""
import netCDF4
import numpy as np
import os
try: from collections import OrderedDict
except ImportError:
    try: from ordereddict import OrderedDict
    except ImportError:
        print "Require OrderedDict, https://pypi.python.org/pypi/ordereddict"
        raise
"""


def ncopen(fname, permission='a', format='NETCDF4_CLASSIC'):
    """
    Return a netCDF object.
    Default permission is 'a' for appending.
    """
    if permission == 'w':
        ncobj = netCDF4.Dataset(fname,mode=permission,format=format)
    else:
        # Format will be deduced by the netCDF modules
        ncobj = netCDF4.Dataset(fname,mode=permission)
    return ncobj


def ncclose(ncobj):
    """
    Close a netCDF object.
    This is required to write the final state of the netCDF object to disk.
    """
    ncobj.close()


def get_attributes(ncobj, verbose=None):
    """
    Copy the global and variable attributes from a netCDF object to an
    OrderedDict.  This is a little like 'ncdump -h' (without the formatting).
    Global attributes are keyed in the OrderedDict by the attribute name.
    Variable attributes are keyed in the OrderedDict by the variable name and
    attribute name separated by a colon, i.e. variable:attribute.

    Some NumPy values returned from the netCDF module are converted to
    equivalent regular types.

    Notes from the netCDF module:
      The ncattrs method of a Dataset or Variable instance can be used to
      retrieve the names of all the netCDF attributes.

      The __dict__ attribute of a Dataset or Variable instance provides all
      the netCDF attribute name/value pairs in an OrderedDict.

      ncobj.dimensions.iteritems()
      ncobj.variables
      ncobj.ncattrs()
      ncobj.__dict__
    """
    d = OrderedDict()

    # Get the global attributes
    d.update( ncobj.__dict__ )

    # Iterate through each Dimension and Variable, pre-pending the dimension
    # or variable name to the name of each attribute
    for name,var in ncobj.variables.iteritems():
        for att,val in var.__dict__.iteritems():
            if verbose: print att,"is value of type",type(val),":",val
            if not isinstance(val,str) and not isinstance(val,unicode):
                #
                # Its probably a numpy dtype thanks to the netCDF* module.
                # Need to convert it to a standard python type for JSON.
                # May need to implement more type checks here.
                # Clues:
                #   isinstance(y,np.float32)
                #   np.issubdtype(y,float)
                #   val = str(val)
                #
                try:
                    if np.issubdtype(val,int):
                        val = int(val)
                        if verbose: print "  Converted to type: int"
                    elif np.issubdtype(val,float):
                        val = float(val)
                        if verbose: print "  Converted to type: float"
                    else:
                        val = string(val)
                        if verbose: print "  Converted to type: string"
                except TypeError:
                    pass
            d.update( {name+':'+att : val} )
    return d


def set_attributes(ncobj, ncdict, delval='DELETE'):
    """
    Copy attribute names and values from a dict (or OrderedDict) to a netCDF
    object.
    Global attributes are keyed in the OrderedDict by the attribute name.
    Variable attributes are keyed in the OrderedDict by the variable name and
    attribute name separated by a colon, i.e. variable:attribute.

    If any value is equal to delval then, if the corresponding attribute exists
    in the netCDF object, the corresponding attribute is removed from the
    netCDF object.  The default value of delval is 'DELETE'. For example,
      nc3_set_attributes(ncobj, {'temperature:missing_value':'DELETE'})
    will delete the missing_value attribute from the temperature variable.

    A ValueError exception is raised if a key refers to a variable name that
    is not defined in the netCDF object.
    """
    # Add metadata attributes
    for k in ncdict.keys():
        p = k.partition(':')
        if p[1]=="":
            # Key is a global attribute
            if ncdict[k]==delval:
                delattr(ncobj, p[0])
            else:
                setattr(ncobj, p[0], ncdict[k])
        elif p[0] in ncobj.variables:
            # Key is a variable attribute
            if ncdict[k]==delval:
                delattr(ncobj.variables[p[0]], p[2])
            elif p[2] == "_FillValue":
                # Its ok to have _FillValue in the dict as long as it has
                # the same value as the variable's attribute
                if getattr(ncobj.variables[p[0]], p[2]) != ncdict[k]:
                    print "Warning: As of netcdf4-python version 0.9.2, _FillValue can only be set when the variable is created (see http://netcdf4-python.googlecode.com/svn/trunk/Changelog). The only way to change the _FillValue would be to copy the array and create a new variable."
                    raise AttributeError("Can not change "+k)
            else:
                setattr(ncobj.variables[p[0]], p[2], ncdict[k])
        else:
            raise ValueError("Variable name in dict does not match any variable names in the netcdf object:", p[0])
    #print "Updated attributes in netcdf object"


def set_timelatlon(ncobj,ntime,nlat,nlon,timeunit=None):
    """
    Create a skeleton 3-D netCDF object with time, latitude and longitude
    dimensions and corresponding dimension variables (but no data in the
    dimension variables).  The dimension variables have 'long_name',
    'standard_name' and 'units' attributes defined.

    Inputs 'ntime', 'nlat' and 'nlon' are the number of elements for the time,
    latitude and longitude vector dimensions, respectively.
    A length of None or 0 (zero) creates an unlimited dimension.

    The default unit for time is: 'days since 1800-01-01 00:00:00.0'.
    The time unit should be in a Udunits format. The time unit and calendar
    (default = gregorian) are used by add_time() to encode a list of
    datetime objects.

    The skeleton object can be customised with the netCDF4 module methods. See
    http://netcdf4-python.googlecode.com/svn/trunk/docs/netCDF4-module.html

    To write data to the dimension variables see add_time() and add_data().

    Recommended ordering of dimensions is:
      time, height or depth (Z), latitude (Y), longitude (X).
    Any other dimensions should be defined before (placed to the left of) the
    spatio-temporal coordinates.

    Examples of adding data to dimensions:
      latitudes[:] = numpy.linspace(-10,-44,681)
      longitudes[:] = numpy.linspace(112,154,841)
      dates = [datetime(2011,2,1)]
      times[:] = netCDF4.date2num(dates,units=times.units,calendar=times.calendar)
    """
    if timeunit==None:
        timeunit = 'days since 1800-01-01 00:00:00.0'

    # Dimensions can be renamed with the 'renameDimension' method of the file
    ncobj.createDimension('time',ntime)
    ncobj.createDimension('latitude',nlat)
    ncobj.createDimension('longitude',nlon)

    times = ncobj.createVariable('time','f8',('time',))
    latitudes = ncobj.createVariable('latitude','f8',('latitude',))
    longitudes = ncobj.createVariable('longitude','f8',('longitude',))

    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    times.long_name = 'time'
    times.standard_name = 'time'
    times.units = timeunit
    times.calendar = 'gregorian'
    times.axis = 'T'


def show_dimensions(ncobj):
    """
    Print the dimension names, lengths and whether they are unlimited.
    """
    print '{0:10} {1:7} {2}'.format("DimName","Length","IsUnlimited")
    for dim,obj in ncobj.dimensions.iteritems():
        print '{0:10} {1:<7d} {2!s}'.format(dim,len(obj),obj.isunlimited())


def set_variable(ncobj, varname, dtype='f4', dims=None, chunksize=None, fill=None, zlib=False):
    """
    Define (create) a variable in a netCDF object.  No data is written to the
    variable yet.  Give the variable's dimensions as a tuple of dimension names.
    Dimensions must have been previously created with ncobj.createDimension
    (e.g. see set_timelatlon()).

    Recommended ordering of dimensions is:
      time, height or depth (Z), latitude (Y), longitude (X).
    Any other dimensions should be defined before (placed to the left of) the
    spatio-temporal coordinates.

    To create a scalar variable, use an empty tuple for the dimensions.
    Variables can be renamed with the 'renameVariable' method of the netCDF
    object.

    Specify compression with zlib=True (default = False).

    Specify the chunksize with a sequence (tuple, list) of the same length
    as dims (i.e., the number of dimensions) where each element of chunksize
    corresponds to the size of the chunk along the corresponding dimension.
    There are some tips and tricks associated with chunking - see
    http://data.auscover.org.au/node/73 for an overview.

    The default behaviour is to create a floating-point (f4) variable
    with dimensions ('time','latitude','longitude'), with no chunking and
    no compression.
    """
    if dims is None:
        dims = ('time','latitude','longitude')
    return ncobj.createVariable(varname, dtype, dimensions=dims,
                          chunksizes=chunksize, fill_value=fill, zlib=zlib)


def add_time(ncobj, datetime_list, timevar='time'):
    """
    Add time data to the time dimension variable.  This routine is separate to
    add_data() because data/time data is encoded in a special way
    according to the units and calendar associated with the time dimension.

    Timelist is a list of datetime objects.

    The time variable should have already been defined with units and calendar
    attributes.

    Examples from:
      http://netcdf4-python.googlecode.com/svn/trunk/docs/netCDF4-module.html

      from datetime import datetime, timedelta
      datetime_list = [datetime(2001,3,1)+n*timedelta(hours=12) for n in range(...)]

      from netCDF3 import num2date
      dates = num2date(nctime[:],units=nctime.units,calendar=nctime.calendar)
    """
    nctime = ncobj.variables[timevar]
    nctime[:] = netCDF4.date2num(datetime_list,
                                 units=nctime.units,
                                 calendar=nctime.calendar)


def add_data(ncobj, varname, data, index=None):
    """
    Add variable data to the given variable name.
    Data can be a list, tuple or NumPy array.

    In general, the size and shape of data must correspond to the size and
    shape of the variable, except when:
    - There are dimension(s) of size 1 defined for the variable, in which
      case these dimension(s) don't need to be included in data.
    - There are unlimited dimension(s) defined for the variable, in which
      case these dimension(s) of the data can be of any size.

    It is also possible to add data to a subset of the variable via slices
    (ranges and strides) passed in through the index parameter.  The only
    caveat to using this feature is that the size and shape suggested by index
    must match the size and shape of data and follow the "In general..." rules
    for the variable as given above.
    This is potentially a very complicated feature.  The netCDF module
    (netCDF4.Variable) will fail if the variable and data dimensions and
    index slices are not compatible.  It is difficult to catch or manipulate
    the inputs to satisfy the netCDF module due to the variety and flexibility
    of this feature.  Experimentation before production is highly recommended.

    Index is a sequence (list,tuple) of slice objects, lists, tuples, integers
    and/or strings.  Each element of the sequence is internally converted to a
    slice object.
    For best results, a sequence of slice objects is recommended so that you
    have explicit control over where the data is placed within the variable.
    The other element types are provided for convenience.

    A slice(None,None,None) object is equivalent to filling the variable with
    the data along the corresponding dimension.  Thus, the default behaviour
    (with index not given) is to fill the variable with data.

    Integers are taken to be the start index.  The end index is then chosen to
    match the data array.  This feature only works if the number of data
    dimensions matches the number of variable dimensions.  Otherwise its too
    hard to guess which data dimension the index integer refers to.
    If an integer element is provided in index, a ValueError will be raised if
    the number of data and variable dimensions do not match.

    Strings are probably only really useful for testing.  Strings are of the
    form "start:stop:stride" with any missing element chosen to be None, i.e.:
      '' or ':' -> slice(None,None,None)
      '2' or '2:' -> slice(2,None,None)
      ':2' -> slice(None,2,None)
      '::2' -> slice(None,None,2)
      '2:4' -> slice(2,4,None)
    """

    # Get the variable object and its shape
    var = ncobj.variables[varname]
    vshp = var.shape  # tuple

    # If data is a list then first convert it to a numpy aray so that
    # the shape can be properly interogated
    if isinstance(data,(list,tuple)):
        data = np.array(data)
    dshp = data.shape  # tuple

    # Fill dshp if required
    # Fill index if required

    # Not quite but close:
    # Loop through vshp, check each dim for either size=1 or unlimited
    # if unlimited, dshp[i]=dshp[i] and index[i]='' or ':'
    # if size=1, dshp[i]=1=vshp[i] and index[i]='' or ':'
    # else
    #    if dshp[i] exists dshp[i]=dshp[i]
    #    else dshp[i]=vshp[i]
    #    if index[i] exists index[i]=index[i]
    #    else index[i]='' or ':'

    if index is None:
        index = ('',) * len(vshp)

    range = []  # List of slice objects, one per dimension
    for i,x in enumerate(index):
        if isinstance(x,slice):
            range.append(x)
        elif isinstance(x,(tuple,list)):
            range.append(slice(x))
        elif isinstance(x,int):
            # Assume x is start index and we slice to the corresponding
            # shape of data
            if len(vshp)==len(dshp):
                # dshp must be same size as vshp for this to work!
                range.append( slice(x,x+dshp[i]) )
            else:
                raise ValueError("Number of dimensions for the data and variable do not match, so I can't guess which data dimension this index refers to. Be explicit with the index range in a slice or string")
        elif isinstance(x,str):
            # Assume its some sort of start:stop:stride string
            p = x.split(':')
            for j in [0,1,2]:
              if j<len(p):
                if p[j]=='': p[j]=None
                else: p[j]=int(p[j])
              else:
                p.append(None)
            range.append(slice(p[0],p[1],p[2]))
        else:
            raise TypeError("Index element is not a valid type: ",x,type(x))

    # Try to add the data to the variable.
    # netCDF4.Variable will complain if dimensions and size ar not valid
    var[range] = data
"""
# Alias functions to support some back compatibility with code that imported
# earlier versions of this file.
def nc_open(*args,**kwargs): return ncopen(args,kwargs)
def nc_close(*args,**kwargs): return ncclose(args,kwargs)
def nc_get_attributes(*args,**kwargs): return get_attributes(args,kwargs)
def nc_set_attributes(*args,**kwargs): return set_attributes(args,kwargs)
def nc_set_timelatlon(*args,**kwargs): return set_timelatlon(args,kwargs)
def nc_show_dims(*args,**kwargs): return show_dims(args,kwargs)
def nc_set_var(*args,**kwargs): return set_var(args,kwargs)
def nc_add_time(*args,**kwargs): return add_time(args,kwargs)
def nc_add_data(*args,**kwargs): return add_data(args,kwargs)
def nc3_open(*args,**kwargs): return ncopen(args,kwargs)
def nc3_close(*args,**kwargs): return ncclose(args,kwargs)
def nc3_get_attributes(*args,**kwargs): return get_attributes(args,kwargs)
def nc3_set_attributes(*args,**kwargs): return set_attributes(args,kwargs)
def nc3_set_timelatlon(*args,**kwargs): return set_timelatlon(args,kwargs)
def nc3_show_dims(*args,**kwargs): return show_dims(args,kwargs)
def nc3_set_var(*args,**kwargs): return set_var(args,kwargs)
def nc3_add_time(*args,**kwargs): return add_time(args,kwargs)
def nc3_add_data(*args,**kwargs): return add_data(args,kwargs)
"""
#
#
####################################################################################

####################################################################################
#
#  SPEDDEXES : netCDF Tools 1.0
#  Authors : Bradley Evans, with code originated by Alex Ip, Jonathan Sixsmith,
#  & Daisy Duursma
#  License : CC-BY 4.0
#  Version : 1.0
####################################################################################
def read_text_file(text_file=''):
    """
    Read text file into a list.
    Removes leading and trailing blanks and new line feeds.

    :param text_file:
        A full file path name to a text file.

    :return:
        A list containing the files to process.
    """
    if os.path.exists(text_file):
        f_open = open(text_file)
        f_read = f_open.readlines()
        f_open.close()
        f_strip = [i.strip() for i in f_read]
        return f_strip
    elif os.path.exists(os.path.join(os.getcwd(),text_file)):
        f_open = open(text_file)
        f_read = f_open.readlines()
        f_open.close()
        f_strip = [i.strip() for i in f_read]
        return f_strip
    else:
        raise Exception('Error. No file with that name exists! filename: %s' %text_file)

def calcLatLonVectors(geoTransform, samples, lines, centre=True, dp=6):
    """
    Calculates the latitude and longitude vectors.

    :param geoTransform:
        A tuple containing the geotransform information returned by GDAL.

    :param samples:
        An integer representing the number of samples (columns) in an array.

    :param lines:
        An integer representing the number of lines (rows) in an array.

    :param centre:
        If True (Default) will account for the upperleft offset and return centre co-ordinates.

    :param dp:
        Set the decimal precision for the returned vectors, default is 6.

    :return:
        A tuple containing the (lat/lon) numpy vectors of type float.
    """
    pix_x = geoTransform[1]
    pix_y = geoTransform[5]

    if centre:
        latvec = numpy.arange(lines) * geoTransform[5] + (geoTransform[3] + (pix_y / 2.))
        lonvec = numpy.arange(samples) * geoTransform[1] + (geoTransform[0] + (pix_x / 2.))
    else:
        latvec = numpy.arange(lines) * geoTransform[5] + geoTransform[3]
        lonvec = numpy.arange(samples) * geoTransform[1] + geoTransform[0]

    return (numpy.round(latvec, dp), numpy.round(lonvec, dp))

def getMinMaxExtents(samples, lines, geoTransform):
    """
    Calculates the min/max extents based on the input latitude and longitude vectors.

    :param samples:
        An integer representing the number of samples (columns) in an array.

    :param lines:
        An integer representing the number of lines (rows) in an array.

    :param geoTransform:
        A tuple containing the geotransform information returned by GDAL.

    :return:
        A tuple containing (min_lat, max_lat, min_lon, max_lat)

    :notes:
        Hasn't been tested for nothern or western hemispheres.
    """
    extents = []
    x_list  = [0,samples]
    y_list  = [0,lines]
    for px in x_list:
        for py in y_list:
            x = geoTransform[0] + (px * geoTransform[1]) + (py * geoTransform[2])
            y = geoTransform[3] + (px * geoTransform[4]) + (py * geoTransform[5])
            extents.append([x,y])

    extents = numpy.array(extents)
    min_lat = numpy.min(extents[:,1])
    max_lat = numpy.max(extents[:,1])
    min_lon = numpy.min(extents[:,0])
    max_lon = numpy.max(extents[:,0])
    return (min_lat, max_lat, min_lon, max_lon)

def dtypeMapping(val):
    """
    Map's GDAL's datatypes to NetCDF4 datatypes.

    :param val:
        An integer representing a GDAL datatype code. If no corresponding value is found then f4 (float32) is returned.

    :return:
        A string containing the netCDF4 datatype code.
    """
    return {
             1 : 'u1',
             2 : 'u2',
             3 : 'i2',
             4 : 'u4',
             5 : 'i4',
             6 : 'f4',
             7 : 'f8',
           }.get(val, 'f4')

def seconds_since_epoch(dt):
    """
    Calculates a seconds since epoch given a datetime variable.

    :param dt:
        A datetime object.

    :return:
        A floating point number representing time as seconds since epoch.
    """
    return (dt - datetime.datetime.utcfromtimestamp(0)).total_seconds()

def locate_files(pattern, root):
    """
    Given a directory, this will walk the directory finding files matching a given pattern.
    :param pattern:
        A string containing the pattern to search for. Eg '*.py'.
    :param root:
        THe root directory from which to start searching.
    :return:
        A list containing files matching the given pattern.
    """
    matches = []
    for path, dirs, files in os.walk(os.path.abspath(root)):
        for filename in fnmatch.filter(files, pattern):
            matches.append(os.path.join(path, filename))
    return matches


####################################################################################
#
#
#  SPEDDEXES : netCDF Tools 2.0
#  Authors : Bradley Evans, with code originated by Alex Ip, Jonathan Sixsmith,
#  & Daisy Duursma
#  License : CC-BY 4.0
#  Version : 1.0
#
####################################################################################

def make_netCDF4CF16_daily  (data=None, \
                             year=None, \
                             month=None, \
                             day=None,  \
                             metadata=None, \
                             outdirectory=None, \
                             outfilename=None, \
                             header=None, \
                             lats=None, \
                             lons=None, \
                             productname=None):
    """
    Makes a netCDF CF file from;
    
    :data:
    A numpy array of the data to be converted
    
    :year:
    Year of the dataset
    
    :month: 
    Month of the dataset
    
    :day:
    Day of the dataset
    
    :metadata: 
    A pandas data frame containing the metadata
    
    :outfilename:
    An output filename
    
    :proj4:
    PROJ.4 description
    http://nsidc.org/data/atlas/epsg_4326.html
     
    """
    # Construct a dict to hold the metadata about the variable
    if metadata!=None:
	
	    variable_name = metadata.loc['standard_name']
	    # get row of data
	    meta2 = {}
	    meta2[variable_name + ':' + 'units'] = str(metadata.loc['ref_identifier_code2'])
	    meta2[variable_name + ':' + 'long_name'] = metadata.loc['id_ci_title'].split(', 1970', 1)[0]
	    meta2[variable_name + ':' + 'cell_methods'] = metadata.loc['temporal_resolution']
	    meta2[variable_name + ':' + 'standard_name'] = variable_name
	    meta2[variable_name + ':' + 'coordinates'] = 'lat lon'
	    meta2[variable_name + ':' + 'grid_mapping'] = 'crs'
	    #Construct a dict to hold the global metadata
	    meta3 = {}
	    meta3['Conventions'] = 'CF-1.6'
	    meta3['title'] = metadata.loc['id_ci_title'].split(', 1970', 1)[0]
	    creation_date = datetime.date.today()
	    meta3['history'] = 'Reformatted to NetCDF %s.'%(creation_date)
	    meta3['license'] = metadata.loc['id_rconstraints_otherconstraints1']
	    meta3['licence_data_access'] = metadata.loc['id_purpose']
	    meta3['spatial_coverage'] = "Australia"
	    meta3['featureType'] = metadata.loc['id_spatial_rep_code']
	    meta3['acknowledgment'] = metadata.loc['id_credit'].split(' Citation: ', 1)[0]
	    meta3['citation'] = metadata.loc['id_credit'].split(' Citation: ', 1)[1]
	    meta3['summary'] = metadata.loc['id_abstract']
	    meta3['contact'] = str(metadata.loc['con_individual_name']) + \
	    ', '+ str(metadata.loc['con_position_name']) + \
	    ', '+ str(metadata.loc['con_addr_deliverypoint'])+ \
	    ', ' + str(metadata.loc['con_addr_city'])+ \
	    ', ' + str(metadata.loc['con_addr_adarea'])  + \
	    ', ' + str(metadata.loc['con_addr_pcode']).zfill(6) + \
	    ', ' + str(metadata.loc['con_addr_country'])+ \
	    ', '+ str(metadata.loc['con_phone_voice'])+ \
	    ', '+  str(metadata.loc['con_addr_email'])+ \
	    ', '+ str(metadata.loc['id_ci_rp_identifier_code'])
	    meta3['institution'] = metadata.loc['con_organisation_name']
	    meta3['references'] = '1. ' +  str(metadata.loc['dq_l_citation_otherDetails1'])+ \
	    ', 2. ' +  str(metadata.loc['dq_l_citation_otherDetails2'])+\
	    ', 3. ' +  str(metadata.loc['dq_l_citation_otherDetails3'])
	    meta3['keywords'] = metadata.loc['id_keywords1']
	    meta3['keywords_vocabulary'] = 'Global Change Master Directory (http://gcmd.nasa.gov)'
	    meta3['source'] = metadata.loc['dq_l_citation_title']
	    meta3['metadata_link'] = 'http://datamgt.nci.org.au:8080/geonetwork'
	    meta3['standard_name_vocabulary'] = 'Climate and Forecast(CF) convention standard names (http://cf-pcmdi.llnl.gov/documents/cf-standard-names)'
	    meta3['id'] = productname
	    meta3['DOI'] = metadata.loc['id_ci_identifier_code']
	    meta3['cdm_data_type'] = metadata.loc['id_spatial_rep_code']
	    meta3['contributor_name'] = str(metadata.loc['id_ci_rp_individualname'])+ ', '+ str(metadata.loc['id_ci_rp_individualname3'])
	    meta3['contributor_role'] = str(metadata.loc['id_ci_rp_role'])+ ', '+ str(metadata.loc['id_ci_rp_role3'])
	    meta3['creator_email']  ='eMAST.data@mq.edu.au'
	    meta3['creator_name']  = 'eMAST data manager'
	    meta3['creator_url']  ='http://www.emast.org.au/'
	    meta3['Metadata_Conventions']="Unidata Dataset Discovery v1.0"
	    meta3['publisher_name']  = str(metadata.loc['id_ci_rp_individualname2'])
	    meta3['publisher_email']  ='eMAST.data@mq.edu.au'
	    meta3['publisher_url']  ='http://www.emast.org.au/'

 
    # Create the datetime for the observation, assume the first day of every month
    #obs_date = datetime.date(int(year), int(month))
    #obs_date = datetime.datetime.combine(obs_date, datetime.datetime.min.time())
    
    # Setup the output directory
    out_dir = os.path.join(outdirectory)
    if (not os.path.exists(out_dir)):
        os.makedirs(out_dir)

    # Output filename
    outfname = os.path.join(out_dir, outfilename)

    # Create the output dataset
    outds = ncopen(outfname, permission='w')        
    #set_timelatlon(outds, 1, header['lines'], header['samples'], timeunit='seconds since 1970-01-01 00:00:00')
    #add_data(outds, 'latitude', lats)
    
    lat   = outds.createDimension('latitude', len(lats))
    lon   = outds.createDimension('longitude', len(lons))

    #times = ncds.createVariable('time','f8',('time',))
    latitudes = outds.createVariable('latitude','f4',('latitude',))
    longitudes = outds.createVariable('longitude','f4',('longitude',))
    #variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',))
    latitudes[:] = lats[:]
    longitudes[:] = lons[:]
    #times[:] = timei[:]





#add_data(outds, 'longitude', lons)
    
    #add_time(outds, [obs_date])
    nodata=-9999
    if nodata != -9999:
        data[data == nodata] = -9999
    nodata = -9999
    #dtype='f4'
    #set_variable(outds, variable_name, dtype=dtypeMapping(band.DataType), fill=nodata)
    #set_variable(outds, productname, dtype=dtypeMapping(d.dtype), fill=nodata)
    variable = outds.createVariable(productname,'f4',('latitude','longitude',), fill_value=nodata)
    variable[:] = data

#    add_data(outds,productname, data)
    #set_attributes(outds, meta2)
    #
    #print outds
    crs_meta = {}
    ref = osr.SpatialReference()
    ref.ImportFromEPSG(header['epsg'])
    set_variable(outds, 'crs', dtypeMapping('f4'), dims=())
    #set_variable(outds, 'crs', dims=())
    new_key = 'crs:name'
    crs_meta[new_key] = ref.GetAttrValue('geogcs')
    new_key = 'crs:datum'
    crs_meta[new_key] = ref.GetAttrValue('datum')
    new_key = 'crs:longitude_of_prime_meridian'
    crs_meta[new_key] = 0.0
    new_key = 'crs:inverse_flattening'
    crs_meta[new_key] = ref.GetInvFlattening()
    new_key = 'crs:semi_major_axis'
    crs_meta[new_key] = ref.GetSemiMajor()
    new_key = 'crs:semi_minor_axis'
    crs_meta[new_key] = ref.GetSemiMinor()
    #
    # TODO : Make this work - to "set" 
    #
    #
    # print crs_meta
    #{'crs:datum': None, 
    #'crs:semi_minor_axis': 6356752.314245179, 
    #'crs:semi_major_axis': 6378137.0, 
    #'crs:name': None, 
    #'crs:longitude_of_prime_meridian': 0.0, 
    #'crs:inverse_flattening': 298.257223563}
    # Write the CRS metadata
    #set_attributes(outds, crs_meta)
    
    # Write the rest of the global metadata
    #set_attributes(outds, meta3)
    
     # Get the extents of the dataset
    #extents = getMinMaxExtents(samples, lines, geoT)

    # Set the extents metadata
    outds.geospatial_lat_min = header['latsmin']
    outds.geospatial_lat_max = header['latsmax']
    outds.geospatial_lat_units = 'degrees_north'
    outds.geospatial_lat_resolution = header['resolution']
    outds.geospatial_lon_min = header['lonsmin']
    outds.geospatial_lon_max = header['lonsmax']
    outds.geospatial_lon_units = 'degrees_east'
    outds.geospatial_lon_resolution = header['resolution']
    #
    ncclose(outds)

    
    
#
##  
####
######
######## MAIN
######
####
##
#
#################################################################################
#  SPEDDEXES : netCDF Tools "MAIN" Function v. 1.0
#  Authors : Bradley Evans
#  License : CC-BY 4.0
#  Version : 1.0
#################################################################################
#
# Req'd 
#meta_csv='/emast/data/watch/metadata/2015-04-29_eMAST_eWATCH_input_metadata_elements.csv'
# <1> List of variables to process including :
#   <1.1> variablename (paired to outdirectory!
#SWdown
years = range(1979,2013)
months=range(1,13)


variablenames=('SWdown', \
              'Tair', \
              'Rainf',\
              'Snowf');
#
productnames=('eMAST_eWATCH_mon_srad_v1m0_1979_2012', \
              'eMAST_eWATCH_mon_tair_v1m0_1979_2012', \
              'eMAST_eWATCH_mon_prec_v1m0_1979_2012',\
              'eMAST_eWATCH_mon_snow_v1m0_1979_2012');
#
#
#
#   <1.2> inputdirectory  ## REMOVE ~ on eMAST-VM machines !!!!!!!!!!
#
#
#  REMOVE tilda (~) when running!
inputDirectories=['/g/data/rr9/eWATCHTemp/srad/', 
              '/g/data/rr9/eWATCHTemp/tair/', 
              '/g/data/rr9/eWATCHTemp/prec/',
              '/g/data/rr9/eWATCHTemp/snow/'
              ];

#   <1.2> outdirectory 
outputDirectories=['/g/data/rr9/eWATCHTemp/mon/srad/', 
              '/g/data/rr9/eWATCHTemp/mon/tair/', 
              '/g/data/rr9/eWATCHTemp/mon/prec/', 
              '/g/data/rr9/eWATCHTemp/mon/snow/']

scalar=(0.036, \
        1.0, \
        86400.0, \
        86400.0);

adj=(0.0, \
     -273.15, \
     0.0, \
     0.0);


#   <1.4> header
#         -- Construct once for all files!
# <2> Loop through available input data (assumption its correct)
#   <2.1> By variablename, input and  outdirectory pairs
for i in range(0,1):
    #
    files=os.listdir(inputDirectories[i])
    #   <2.1****> Open METADATA file, extract matching set
    #          -- load metadata for a product ONCE
    #
    #read in metadata file
    #meta = pandas.io.parsers.read_csv(meta_csv,index_col = 'fileidentifier')
    #subset metadata based on parameter name
    #metadata = meta.loc[productname[i]]
    for filename in files:
        fname    = os.path.basename(filename)
        print('File Name: :' +fname)
	year  = fname[-9:-5]
        print('Year :' +year)
	month  = fname[-5:-3]
	print ('Month: ' + month)
        #print(filename)
        #   <2.2> Open WATCH data file
        fullpath=inputDirectories[i]+filename
        print('Full path: ' + fullpath)
	f = netcdf.NetCDFFile(fullpath, "r")
        print(f.dimensions)
	print(f.variables)
	# Cast it as an numpy arrary
        data = np.array(f.variables[variablenames[i]].data)
#	data=data.max(axis=0)
	print(variablenames[i])	
	print('Original data array')
	print(type(data))
	print(data.shape)
	print(data)
        d=""
	print('Changing data')
	d=data
        #d[d>=1.00000002e+20]= -9999.0
        d[d==1.00000002e+20]=np.nan
	d=(d*scalar[i])+adj[i]
        #di[d>=(1.00000002e+20*scalar[i])+adj[i]]= -9999.0
	print('Changing missing values to none')
	#d[d==(1.00000002e+20*scalar[i])+adj[i]]= None
	print(d)

	# number of timesteps in 1 day
	
	hoursInTimeStep = 3
	hoursInDay =24
	timeStepsPerDay = hoursInDay / hoursInTimeStep
	timeStepsInDataFrame = d.shape[0]
	print('Number of timesteps: '+ str(timeStepsInDataFrame))
	numberOfDays = timeStepsInDataFrame / timeStepsPerDay
	print('Number of days in month: '+ str(numberOfDays))
	d= d.reshape(8,numberOfDays,360,720)
	print(d.shape)
	d = d.mean(axis=0)*3
	print(d.shape)
	print(d)
	if i==1:
		print('Getting Mean')
		d=d.sum(axis=0)*3
		print(d)
	else:
		print('Getting Sum')
		d=d.sum(axis=0)
	
	print('Replacing missing vvalues with -9999.0')
	d[np.isnan(d)]=-9999.0	
        print(d.shape)
        print(d)

	# Extract the grid dimensions &             
        #   <2.3> Build projection dictionary 
        
	lats=np.array(f.variables['lat'].data)
        print(type(lats))
	print(lats.shape)
	#print(lats.shape())
	print(lats)
	
	lons=np.array(f.variables['lon'].data)
        print(lons.shape)
	print(type(lats))
	print(lons)
	
	x, y = (lons, lats) 
        header = { \
          'bands': 1, \
          'latsmin': min(lats), \
          'lonsmin':min(lons), \
          'latsmax': max(lats), \
          'lonsmax':max(lons), \
          'epsg':4326, \
          'resolution':0.5}
        # <2.3> Loop through each of the "years" 
        #
        # Should extract number of days to iterate over!
       # mmonth=data.shape[0]
       # print(str(data))
#	print('mmonth is:')
#	print(mmonth)
	
	# Loop through the dats
 #       for month in xrange(1,mmonth):
       # d=data
    #d[d>=1.00000002e+20]= -9999.0
   #	d=(d*scalar[i])+adj[i]
    #  	d[d>=(1.00000002e+20*scalar[i])+adj[i]]= -9999.0
 #	print(data.shape)
#	print(data)
	  # d ays='01'
	#mmonth=data.shape[0]
        # Loop through the dats
        #for month in xrange(1,mmonth):
        #   d=data[month]
            #d[d>=1.00000002e+20]= -9999.0

       
            #outfilename= productname[i] +"_" +year +( month+1) + days +'.nc'
            #newdate= '%s%s%s' %(year,month,days)
            #today.strftime('%Y%m%y)
    #newdate= '%s%s%s' %(year,month,days)
    #today.strftime('%Y%m%y)
   # outfilename= '%s_%s%s%s.nc' %(productname[i],year,month,"{0}".format(days))
    	outfilename= '%s_%s%s.nc' %(productnames[i],year,month)
        print('Outfile name: ' +outfilename)
    
    #outfilename= '%s_%s%s%s.nc' %(productname[i],year,month,"{:0>2d}".format(days))
    #  <3> Call the function
    #

        make_netCDF4CF16_daily  (data=d, year=year,month=month, \
			     day=None,  \
			     metadata=None, \
			     outdirectory=outputDirectories[i], \
			     outfilename=outfilename, \
			     header=header, \
			     lats=lats, \
			     lons=lons, \
			     productname=productnames[i])





    

