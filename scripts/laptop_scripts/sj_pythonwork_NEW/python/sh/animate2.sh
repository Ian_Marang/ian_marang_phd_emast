#!/bin/bash
#PBS -l ncpus=1
#PBS -l walltime=00:10:00
#PBS -l mem=16GB
#PBS -P xa5
#PBS -q express

module load hdf5/1.8.10
module load netcdf/4.2.1.1
module load python/2.7.3-matplotlib
module load gdal
module load proj
module use /projects/xa5/modules
module load pythonlib/netCDF4/1.0.4

convert -delay 50 /g/data/rr9/sjj576/visualisations/Terrestrial_Ecosystems/Climate/eMAST/eMAST_eWATCH/0_5deg/v1m1/mon/land/alph/e_01/1979_2012/pngLarge/* /g/data/rr9/sjj576/visualisations/Terrestrial_Ecosystems/Climate/eMAST/eMAST_eWATCH/0_5deg/v1m1/mon/land/alph/e_01/1979_2012/pngLarge/eMAST_eWATCH_mon_alph_v1m1_1979_2012.gif


