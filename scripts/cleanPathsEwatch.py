import eMAST.path_maker as pm
import numpy as np
import pandas as pd

variables = ['eMAST_mon_alph_v1m1_1970_2012'
,'eMAST_mon_parr_v1m1_1970_2012',
'eMAST_mon_etpo_v1m1_1970_2012',
'eMAST_mon_runo_v1m1_1970_2012',
'eMAST_mon_gd10_v1m1_1970_2012',
'eMAST_mon_gd05_v1m1_1970_2012',
'eMAST_mon_gd00_v1m1_1970_2012',
'eMAST_mon_eteq_v1m1_1970_2012',
'eMAST_mon_moin_v1m1_1970_2012',
'eMAST_mon_chil_v1m1_1970_2012',
'eMAST_mon_etac_v1m1_1970_2012']


variables1 =['eMAST_eWATCH_mon_gd10_v1m1_1979_2012',
'eMAST_eWATCH_mon_gd05_v1m1_1979_2012',
'eMAST_eWATCH_mon_gd00_v1m1_1979_2012',
'eMAST_eWATCH_mon_chil_v1m1_1979_2012',
'eMAST_eWATCH_mon_alph_v1m1_1979_2012',
'eMAST_eWATCH_mon_moin_v1m1_1979_2012',
'eMAST_eWATCH_mon_parr_v1m1_1979_2012',
'eMAST_eWATCH_mon_etpo_v1m1_1979_2012',
'eMAST_eWATCH_mon_fsun_v1m1_1979_2012',
'eMAST_eWATCH_mon_eteq_v1m1_1979_2012',
'eMAST_eWATCH_mon_etac_v1m1_1979_2012',
'eMAST_eWATCH_mon_runo_v1m1_1979_2012']


pm.createMultipleStagingDirectories(variables1)



