import eMAST.path_maker as pm
import numpy as np
import pandas as pd

variables = ['eMAST_mon_alph_v1m1_1970_2012'
,'eMAST_mon_parr_v1m1_1970_2012',
'eMAST_mon_etpo_v1m1_1970_2012',
'eMAST_mon_runo_v1m1_1970_2012',
'eMAST_mon_gd10_v1m1_1970_2012',
'eMAST_mon_gd05_v1m1_1970_2012',
'eMAST_mon_gd00_v1m1_1970_2012',
'eMAST_mon_eteq_v1m1_1970_2012',
'eMAST_mon_moin_v1m1_1970_2012',
'eMAST_mon_chil_v1m1_1970_2012',
'eMAST_mon_etac_v1m1_1970_2012']



input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/parr/e_01/1970_2012'
variable = 'eMAST_mon_parr_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)

input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/etpo/e_01/1970_2012'
variable = 'eMAST_mon_etpo_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)

input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/runo/e_01/1970_2012'
variable = 'eMAST_mon_runo_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)

input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/gd10/e_01/1970_2012'
variable = 'eMAST_mon_gd10_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)


input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/gd05/e_01/1970_2012'
variable = 'eMAST_mon_gd05_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)


input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/gd00/e_01/1970_2012'
variable = 'eMAST_mon_gd00_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)


input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/parr/e_01/1970_2012'
variable = 'eMAST_mon_parr_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)


input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/eteq/e_01/1970_2012'
variable = 'eMAST_mon_eteq_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)


input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/moin/e_01/1970_2012'
variable = 'eMAST_mon_moin_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)

input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/chil/e_01/1970_2012'
variable = 'eMAST_mon_chil_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)

input = '/g/data/rr9/temp40yearsFix2/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1/mon/land/eteq/e_01/1970_2012'
variable = 'eMAST_mon_eteq_v1m1_1970_2012'

pm.moveDirectoryContentsToStaging(input,variable)




