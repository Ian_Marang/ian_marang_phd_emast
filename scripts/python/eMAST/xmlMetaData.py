import xmlHandler as xml
import urllib2 as url2

class xmlMetaData:
	def __init__(self,oai_pmh,fileIdentifier,prefixSelection):
		self.oai_pmh =oai_pmh
		self.fileIdentifier = fileIdentifier
		self.prefixSelection = prefixSelection 
		
		print('Getting metadata from %s for %s') % (oai_pmh.url,self.fileIdentifier)
                self.metaDataQuery='%s?verb=GetRecord&identifier=%s&metadataPrefix=%s' % (oai_pmh.url, fileIdentifier,prefixSelection)
                response = url2.urlopen(self.metaDataQuery)
		
                # create xml file from response object
                xmlObjTool=xml.xmlObjTools(response.read())
		
		#print(xmlObjTool.xmlFileText)
                
		# get meta data 
		self.metadataXML = xmlObjTool.getMetaDataXML('metadata','iso19139.anzlic','http://www.isotc211.org/2005/gmd')
               	 
