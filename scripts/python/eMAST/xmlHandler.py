from xml.dom import minidom


class xmlObjTools:
	
	def __init__(self,xmlFileText):
		self.xmlFileText = xmlFileText
		self.xmlObj = minidom.parseString(self.xmlFileText) 
		
	def convertXMLToList(self,tagName):
		print('Creating list from xml file')
	        values = []
		for node in self.xmlObj.getElementsByTagName(tagName):
			try:
				values.append(node.firstChild.data)
			except:
				values.append(None)
		return values

	def convertListToDictionary(self,keys,values):
		print('Converting 2 lists to ordered dictionary')
		dictionary = {}
		listLength = len(keys)	
		for i in range (0,listLength):
			dictionary[keys[i]]=values[i]
		return dictionary
			

	def getMetaDataXML(self,parentNode,prefix,URI):
		
		print('Getting Meta data in XML format')
		
		# get all nodes including the parent
		metaDataList=self.xmlObj.getElementsByTagName(parentNode).item(0)
		print(metaDataList.toxml())	
	        
		
		## get child nodes of parent node (remove <metadata> tag from XML
		metaDataList=metaDataList.childNodes
	   	print(metaDataList)			
		
		# remove text nodes
		removeNode=[]
		for node in metaDataList:
			print(node.nodeType)
	
			if node.nodeType == 3:
				removeNode.append(node)
		
		for node in removeNode:
			print(node)
			metaDataList.remove(node)
		metaDataXML = metaDataList.item(0)
		print(metaDataXML.toxml())
		
		# get meta data for the namespace URI only
		prefix=metaDataXML.prefix
		nameSpaceURI=metaDataXML.namespaceURI	
		
		metaData=metaDataXML.getElementsByTagNameNS(nameSpaceURI,'*')		
		
		for node in metaData:
			print(node.data)	
		print(metaData)
		#print (metaData[0].toxml())
		#for node in metaData:
		#	print node.toxml()
		return metaData
		
	def convertXMLToOrderedDictionary(self,parent):
		print('Converting xml to ordered dictoinary')
