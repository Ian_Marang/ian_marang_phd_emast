def netCDF4Converter(dataProductDictionary):
        #get directory structure based on the model
        directoryStructureList=getDirectoryStructure(model)
        for product in dataProductDictionary.keys():
                #call function to obtain metadata for product , need to get metadata becuase the variable name is in there
                productMetaData = getMetaData(metaDataDataFrame,product, dataProductDictionary)
                print(productMetaData)
                print(product)
                #iterate through year   
                for year in yearRangeList:
                        print(year)
                        # call function that generates directory 
                        directory = generateDirectory(directoryStructureList,product)
                        # iterate through month 
                        for month in months:
                                print(month)
                                # call function to generate filename

                                fileNameInput = generateFileName(directoryStructureList[3],product,directoryStructureList[4],year,month,directoryStructureList[5])
                                pathInput = directory + fileNameInput
                                #check if there is any meta data to write
                                if productMetaData !={}:
                                        print('Opening netCDF File:' + pathInput)
                                        netCDFObj = ncopen(pathInput, permission='r')
                                        print(netCDFObj)
                                        # call getVariables
                                        variableDict=getVariablesDict(netCDFObj,productMetaData['long_name'])
                                        # check to see if time variable is added, if not add in
                                        variableDict=addTimeVariable(year,month,variableDict)
                                        # generate output path
                                        outputPath = directory + generateOutputPath(directoryStructureList,product,year,month)
                                        print('Complete output path is: '+ outputPath)
                                        #call function to get header dictionary
                                        headerDict = generateHead(variableDict['longitude'],variableDict['latitude'])

                                        # Run the make netCDF function
                                        make_nc(outfile=outputPath, varname=productMetaData['long_name'], data=variableDict[productMetaData['long_name']],lati=variableDict['latitude'], loni=variableDict['longitude'],timei=variableDict['time'], header=headerDict)

def getVariablesDict(netCDFObj,productName):
        print(netCDFObj)
        for name in netCDFObj.ncattrs():
                print 'Global attr', name, '=', getattr(netCDFObj,name)

        variableDict = netCDFObj.variables
        variableArrayDict = {}
        print('Getting file variables: ')
        print(variableDict)
        for variable in variableDict.keys():
                if variable =='latitude':
                        print('Getting latitude variables')
                        lato= np.array(netCDFObj.variables['latitude'])
                        variableArrayDict['latitude']=lato
                if variable =='longitude':
                        print('Getting longitude variables')
                        lono=np.array(netCDFObj.variables['longitude'])
                        variableArrayDict['longitude']=lono
                if variable =='time':
                        print('Getting Time variable')
                        time =np.array(netCDFObj.variables['time'])
                        variableArrayDict['time']=time
                if variable != 'latitude' and variable!='longitude' and variable !='time':
                        print('Getting Variable: '+ variable)
                        # need to obtain the key for the variable, becuase this will be different each time
                        variableName = np.array(netCDFObj.variables[variable])
                        print(variableName)
                        print('Min Value: ')
                        print( np.amin(variableName))
                        print('Max Value: ')
                        print(np.amax(variableName))
                        if np.amin(variableName) ==np.amin(variableName):
                                print('Replacing missing values with -9999')
                                variableName[variableName==np.amin(variableName)]=-9999
                                print(variableName)
                                print('Min Value: ')
                                print(np.amin(variableName))
                                print('Max Value: ')
                                print(np.amax(variableName))
                        variableArrayDict[productName]= variableName
        print(variableArrayDict)
        return (variableArrayDict)

def addTimeVariable(year,month,variableDict):
        print('Adding time variable')
        print(variableDict['latitude'].shape)
        if 'time' not in variableDict.keys():
                if model =='emast':
                #       if month>=10:
                #               month='0'+str(month)
                #       date=str(year)+'-'+str(month)+'-'+'01'          
                        date = datetime(year=year, month=month,day=1)
                if model =='ANUClim':
                # date=str(year)+'-01-01'
                        date=datetime(year=year,month=1,day=1)
                date = date.toordinal()
                time = np.zeros(variableDict['latitude'].shape)
                time[time==0] = date
                print(time.shape)
                variableDict['time']=time
        print(variableDict)
        return (variableDict)




