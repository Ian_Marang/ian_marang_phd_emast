#!/bin/bash
#PBS -l ncpus=16
#PBS -l walltime=10:00:00
#PBS -l mem=126GB
#PBS -P xa5
#PBS -q normal

module load hdf5/1.8.10
module load netcdf/4.2.1.1
module load python/2.7.3-matplotlib
module unload python/2.7.3-matplotlib
module unload python/2.7.3
module load python/2.7.6
module load python/2.7.6-matplotlib
module load gdal
module load proj
module use /projects/xa5/modules
module load pythonlib/netCDF4/1.0.4

python /g/data/rr9/IM_PhD/scripts/python/IM_anom_timeseries_2000_2010.py 
#python /home/576/sjj576/bitbucket/eMAST/python/eMAST/stash_grid_v01.py > /home/576/sjj576/bitbucket/eMAST/python/sh/output/runModel.output                                                                        
