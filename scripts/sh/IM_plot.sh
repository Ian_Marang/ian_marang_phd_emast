#!/bin/bash
#PBS -l ncpus=1
#PBS -l walltime=04:00:00
#PBS -l mem=16GB
#PBS -P xa5
#PBS -q copyq

module load hdf5/1.8.10
module load netcdf/4.2.1.1
module load python/2.7.3-matplotlib
module load python/basemap
module load gdal
#module load proj
#module use /projects/xa5/modules
module load pythonlib/netCDF4/1.0.4

python /home/576/ijm576/job_scripts/Plot_NT_mean_prec_single.py
