#!/bin/bash
#PBS -P xa5
#PBS -q express
#PBS -l walltime=00:30:00
#PBS -l mem=32gb
#PBS -l ncpus=8
#PBS -lother=gdata1

cd /g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1_aus/yr/land/swco/e_01/1970_2012

for f in file*; do mv $f ${f/${f:1:4}/}; done
