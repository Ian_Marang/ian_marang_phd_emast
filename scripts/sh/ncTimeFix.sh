#!/bin/bash
#PBS -M ian.marang\@students.mq.edu.au
#PBS -P xa5
#PBS -q normal
#PBS -l walltime=03:00:00
#PBS -l mem=64gb
#PBS -l ncpus=16
#PBS -lother=gdata1


cd /g/data/rr9/IM_PhD/scripts/
python ncTimeFix.py
