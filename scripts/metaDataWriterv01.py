# import modules
import numpy as np
import pandas as pd


#global variables
pathToMetaDataCSV = ""
metaDataFileName = "metadata.csv"
completePath = pathToMetaDataCSV+ metaDataFileName
print(completePath)

#directory structures
basePath='/g/data/rr9/temp/'
path1 = '/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/'
path2='/e_01/1970_2012/'

# file structure
file1 ='eMAST_ANUClimate_mon_'
file2='_v1m0_'
file3='_v0.nc'


dataProductsDictionary = { 
   'etac':'',
   'etet':'',
   'etpo':'',
   'etde':'',
   'parr':'',
   'mind':'',
   'alph':'eMAST_mon_alph_v1m1_1970_2012',
   'gd00':'eMAST_mon_gd00_v1m1_1970_2012',
   'runo':'',
   'gd05':'eMAST_mon_gd05_v1m1_1970_2012',
   'gd10':'eMAST_mon_gd10_v1m1_1970_2012',
   'chil':'eMAST_mon_chil_v1m1_1970_2012',
}


#configurations
startYear =1970
endYear = 2012
yearRangeList = range(startYear, endYear)
print(yearRangeList)
monthsToProcess = 12
months =range(1,monthsToProcess)

# create dataframe from metadata csv
metaDataDataFrame=pd.DataFrame.from_csv(completePath)
print(metaDataDataFrame.head())


# function that iterates through every value in key and find relevant line in data frame
#iterate through list
def metaDataWriter(dataProductDictionary):
	for product in dataProductsDictionary.keys():
	        #call function to obtain metadata for product
		productMetaData = getMetaData(metaDataDataFrame,product, dataProductDictionary)
                print(productMetaData)
		print(product)
                #iterate through year	
		for year in yearRangeList:
                        print(year)
                	# call function that generates directory 
                  	directory = generateDirectory(basePath,path1,product,path2)
			# iterate through month 
                        for month in months:
                  		print(month)
                                # call function to generate filename
				fileName = generateFileName(file1,product,file2,year,month,file3)
				# call function to update metadata
                                updateMetaData(directory, fileName, dataProductsDictionary)

# function to uptate metadata by taking location as argument,  opening netcdf file, writing metadata, closing netdcdf

def getMetaData(metaDataDataFrame,product,dataProductDictionary):
	dataProductDict={}
	print('Getting Metadata')	
	print(product)
	print(dataProductDictionary)        
	# find the dataProduct index using the product
	dataProductID=dataProductDictionary[product]	
	print('Print data product ID: '+ dataProductID)
        
        # using the dataProduct index, save a dictionary of all the columns for that row
	print(metaDataDataFrame.index)	
       
        if dataProductID!='': 
		dataProductSeries= metaDataDataFrame.loc[dataProductID, :]	
		print(dataProductSeries)
                print(type(dataProductSeries))
	        dataProductDict=dataProductSeries.to_dict()
		print(type(dataProductDict))
	return(dataProductDict)
	# return dictionary of metadata
	


def updateMetaData(directory,fileName,dataProductDictionary):
	print('Updating Metadata')

# function that generates directory
# example directory /g/data/rr9/temp/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/etac/e_01/1970_2012/

def generateDirectory(basePath,path1,variableName,path2):
	print('Generating directory name')
	directoryName = basePath + path1+ variableName + path2	
        print (directoryName)
        return directoryName

# function to generate filename
# example filename eMAST_ANUClimate_mon_etac_v1m0_197009_v0.nc 
def  generateFileName(file1,product,file2,year,month,file3):
	print('Generating File Name')
        if month<10 :
		month= '0'+str(month)
	fileName = file1+product+file2+str(year)+str(month)+file3
	print(fileName)
	return(fileName)


# run functions
metaDataWriter(dataProductsDictionary)




