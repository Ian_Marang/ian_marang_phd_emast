# -*- coding: utf-8 -*-
#Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
import os, sys
import os.path
from fnmatch import fnmatch
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, cm
import eMAST.path_maker as pm
import pandas as pd
import eMAST.metadata as md
from numpy import genfromtxt
import eMAST.netCDFTools as ncdf

nodata=-9999.
nameVar=[]
# Get a list of the files to loop through
os.chdir('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/')

l = Dataset('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012/eMAST_ANUClimate_mon_prec_v1m0_197012.nc')
lon1 = l.variables['longitude']
lat1 = l.variables['latitude']


# Define function for reading ncfile
def read_ncdata(infile=None, varname=None):
    """ Returns data from netcdf file as outdata for use in other functions
    Requires an infile = filename and a varname = variable """
    g = Dataset(infile,'r')
    outdata = np.array(g.variables[varname])
    outdata[outdata == nodata] = decimal.Decimal("NaN")
    return outdata;

# Loop through files, append to array
def makeMonthArray(month):
    itr = 0
    for root, dirs, files in os.walk(".",topdown=True):
        for name in files:
            if fnmatch(name,'*_v1m0_????'+str(month).zfill(2)+'.nc'):
                if itr==0:
                    prec_data = read_ncdata(name,'lwe_thickness_of_precipitation_amount')
                else:
                    new_data = read_ncdata(name,'lwe_thickness_of_precipitation_amount')
                    prec_data = np.vstack((prec_data,new_data))
                itr=itr+1
                print(np.shape(prec_data))
                continue
        return prec_data

def makeMonthMean(data,month):
    mean=np.mean(data,axis=0)
    print('Making mean of: %s'%month)
    print np.mean(mean)
    return mean

def makeMonthStd(data,month):
    std=np.nanstd(data,axis=0)
    print('Making std of: %s'%month)
    print np.mean(std)
    return std

def makeMonthAnom(mean,std,month):
    itr = 0
    for root, dirs, files in os.walk(".",topdown=True):
        for name in files:
            if fnmatch(name,'*_v1m0_????'+str(month).zfill(2)+'.nc'):
                nameVar=[]
                nameVar.extend(str.split(name,'_'))
                print('Processing anomalies for: %s'%month)
                data=np.squeeze(read_ncdata(name,'lwe_thickness_of_precipitation_amount'))
                adata=(data-mean)/std
                adata=np.expand_dims(adata,0)
                make_nc(outfile='/g/data/rr9/IM_PhD/data/monthly_anomalies/prec/Test_anoms_'+nameVar[-1],
                        varname='lwe_thickness_of_precipitation_amount',
                        data=adata,
                        lati=lat1,
                        loni=lon1,
                        header=head,
                        nodata=-9999.)
                print adata.shape
                nameVar=[]

def make_nc(outfile=None, varname=None, data=None, lati=None, loni=None, header=None, nodata=-9999.):
    ncds = Dataset(outfile, 'w', format='NETCDF4_CLASSIC', zlib=True)
    time  = ncds.createDimension('time', None)
    lat   = ncds.createDimension('latitude', len(lati))
    lon   = ncds.createDimension('longitude', len(loni))
    times = ncds.createVariable('time','f8',('time',)) 
    latitudes = ncds.createVariable('latitude','f4',('latitude',))
    longitudes = ncds.createVariable('longitude','f4',('longitude',))
    variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',), fill_value=nodata) 
    #variable = ncds.createVariable(varname,'f4',('latitude','longitude',), fill_value=nodata) 
    latitudes[:] = lati[:]
    longitudes[:] = loni[:]
    variable[:,:,:] = data # time version of the data insert 
    #variable[:,:] = data 
    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    # Removed time variable - add as required
    times.long_name = 'time' 
    times.standard_name = 'time' 
    times.units = 'month' 
    times.calendar = 'gregorian' 
    times.axis = 'T' 
    # Set geospatial global methods [basic method for setting global attr.]
    ncds.geospatial_lat_min = header['latsmin']
    ncds.geospatial_lat_max = header['latsmax']
    ncds.geospatial_lat_units = 'degrees_north'
    ncds.geospatial_lat_resolution = header['resolution']
    ncds.geospatial_lon_min = header['lonsmin']
    ncds.geospatial_lon_max = header['lonsmax']
    ncds.geospatial_lon_units = 'degrees_east'
    ncds.geospatial_lon_resolution = header['resolution']
    crs=ncds.createVariable('crs','f4')                                      
    # Set crs as variable
    crs.name="GDA94"
    crs.datum= "Geocentric_Datum_of_Australia_1994"
    crs.longitude_of_prime_meridian= 0.0
    crs.inverse_flattening=298.257222101
    crs.semi_major_axis=6378137.0
    crs.semi_minor_axis=6356752.314140356
    crs._CoordinateTransformType="Projection"
    crs._CoordinateAxisTypes="GeoX GeoY"
    # Set crs globally [alternative method!]
    setattr(ncds, 'crs:name', "GDA94")
    setattr(ncds, 'crs:datum', "Geocentric_Datum_of_Australia_1994")
    setattr(ncds, 'crs:longitude_of_prime_meridian', 0.0)
    setattr(ncds, 'crs:inverse_flattening', 298.257222101)
    setattr(ncds, 'crs:semi_major_axis', 6378137.0)
    setattr(ncds, 'crs:semi_minor_axis', 6356752.314140356)
    setattr(ncds, 'crs:_CoordinateTransformType', "Projection")
    setattr(ncds, 'crs:_CoordinateAxisTypes', "GeoX GeoY")
    # Close the file
    ncds.close()
    # Report back
    print 'Congratulations, your netCDF file is baked! See:', outfile

# Set ncfile header
x, y = (lon1, lat1) 
head = {'samples': len(x), \
  'lines': len(y), \
  'bands': 1, \
  'latsmin': min(lat1), \
  'lonsmin':min(lon1), \
  'latsmax': max(lat1), \
  'lonsmax':max(lon1), \
  'epsg':4326, \
  'resolution':0.01}
                
for i in range(1,13):
    pdata = makeMonthArray(i)
    print('Shape of pdata in for loop: %s'%str(np.shape(pdata)))
    mean = makeMonthMean(pdata,i)
    std = makeMonthStd(pdata,i)
    adata=makeMonthAnom(mean,std,i)
    