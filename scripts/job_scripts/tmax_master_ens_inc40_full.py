# Final working ensemble generator - Max Temperature - Full Time Series 1970-2012
# Uses os.walk to find file location
# Gives mean for whole time series
# Gives std deviation, variance and std error of the mean with +/- 40 increments for each month
# Gives 80 random perturbation each month
# IN for loops nested to provide: (for pos inc) i) data step, j) ens index start at 00
#                                 (for neg inc) i) data step, j) neg inc, k) ens index start at 00
# OUT for loops nested to provide: (for both inc) i) data step, j) ens index start at 00 


#Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
import os, sys
import os.path
from scipy import stats

# Set nodata value
nodata = -9999.

# Define function for reading ncfile
def read_ncdata(infile=None, varname=None):
    """ Returns data from netcdf file as outdata for use in other functions
    Requires an infile = filename and a varname = variable """
    g = Dataset(infile,'r')
    outdata = np.array(g.variables[varname])
    outdata[outdata == nodata] = decimal.Decimal("NaN")
    return outdata;

# change directory for os.walk
os.chdir('/g/data1/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/tmax/e_01/1970_2012/')

# Define Iterator count for array creation during loop through files
itr=0

# Loop through files, append to array
for root, dirs, files in os.walk(".",topdown=True):
    for name in files:
        if name.endswith('.nc'):
            if itr==0:
                tmax_data = read_ncdata(name,'air_temperature')
            else:
                new_data = read_ncdata(name,'air_temperature')
                tmax_data = np.vstack((tmax_data,new_data))
            itr=itr+1

# Mean - In
# Find Mean across months (ignoring NaN)
tmax_mean = np.mean(tmax_data, axis=0)

# Standard Deviation - In
# Find standard deviation increment(ignoring NaN)
tmax_std_inc = (np.nanstd(tmax_data, ddof=1, axis=0))/20

# Generate products for each month + 40 standard deviation increments
for i in range(0,504):
    for j in range(1,41):
        perturb = tmax_data[i,:,:]+(tmax_std_inc*(j))
        vars()['tmax_std_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)] = perturb

# Generate products for each month - 40 standard deviation increments
for i in range(0,504):
    for j in range(1,41):
        for k in range(40,80):
            perturb1 = (tmax_data[i,:,:]+(tmax_std_inc*-1*(j))).clip(0)
            vars()['tmax_std_'+str(((i+1)/12)+1970)+str((i+1)%12)+'_ens'+str(k).zfill(2)] = perturb1


# Variance - In
# Find variance increment
tmax_var_inc = (np.nanvar(tmax_data, ddof=1, axis=0))/20

# Generate products for each month + 40 variance increments
for i in range(0,504):
    for j in range(1,41):
        perturb = tmax_data[i,:,:]+(tmax_std_inc*(j))
        vars()['tmax_var_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)] = perturb

# Generate products for each month - 40 variance increments
for i in range(0,504):
    for j in range(1,41):
        for k in range(40,80):
            perturb1 = (tmax_data[i,:,:]+(tmax_var_inc*-1*(j))).clip(0)
            vars()['tmax_var_'+str(((i+1)/12)+1970)+str((i+1)%12)+'_ens'+str(k).zfill(2)] = perturb1


# Standard Error of the Mean - In
# Find standard error of the mean
tmax_sem_inc = (stats.sem(tmax_data, axis=0))/20

# Generate products for each month + 40 standard error of the mean increments
for i in range(0,504):
    for j in range(1,41):
        perturbs = tmax_data[i,:,:]+(tmax_sem_inc*(j))
        vars()['tmax_sem_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)] = perturbs

# Generate products for each month - 40 standard error of the mean increments
for i in range(0,504):
    for j in range(1,41):
        for k in range(40,80):
            perturbs1 = (tmax_data[i,:,:]+(tmax_sem_inc*-1*(j))).clip(0)
            vars()['tmax_sem_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(k).zfill(2)] = perturbs1


# Random perturbations - In
# Assign mean and standard deviation for perturbation
mu, sigma = 0, 10e-11
s = np.random.normal(mu, sigma, 80)

for i in range(0,504):
    for j in s:
        for k in range(1,81):
            perturbr = (tmax_data[i,:,:]+(tmax_data[i,:,:]*j)).clip(0)
            vars()['tmax_ran_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(k).zfill(2)] = perturbr


# Get lats and lons from sample file
a = Dataset('/g/data1/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/tmax/e_01/1970_2012/eMAST_ANUClimate_mon_tmax_v1m0_197001.nc')
lat1 = a.variables['latitude'][:]
lon1 = a.variables['longitude'][:]

# Function to write an eMAST formatted netCDF file - example usage below...

def make_nc(outfile=None, varname=None, data=None, lati=None, loni=None, header=None, nodata=-9999.):
    ncds = Dataset(outfile, 'w', format='NETCDF4_CLASSIC', zlib=True)
    time  = ncds.createDimension('time', None)
    lat   = ncds.createDimension('latitude', len(lati))
    lon   = ncds.createDimension('longitude', len(loni))
    #times = ncds.createVariable('time','f8',('time',)) 
    latitudes = ncds.createVariable('latitude','f4',('latitude',))
    longitudes = ncds.createVariable('longitude','f4',('longitude',))
    #variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',), fill_value=nodata) 
    variable = ncds.createVariable(varname,'f4',('latitude','longitude',), fill_value=nodata) 
    latitudes[:] = lati[:]
    longitudes[:] = loni[:]
    #variable[:,:,:] = data # time version of the data insert 
    variable[:,:] = data 
    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    # Removed time variable - add as required
    #times.long_name = 'time' 
    #times.standard_name = 'time' 
    #times.units = 'month' 
    #times.calendar = 'gregorian' 
    #times.axis = 'T' 
    # Set geospatial global methods [basic method for setting global attr.]
    ncds.geospatial_lat_min = header['latsmin']
    ncds.geospatial_lat_max = header['latsmax']
    ncds.geospatial_lat_units = 'degrees_north'
    ncds.geospatial_lat_resolution = header['resolution']
    ncds.geospatial_lon_min = header['lonsmin']
    ncds.geospatial_lon_max = header['lonsmax']
    ncds.geospatial_lon_units = 'degrees_east'
    ncds.geospatial_lon_resolution = header['resolution']
    crs=ncds.createVariable('crs','f4')                                      
    # Set crs as variable
    crs.name="GDA94"
    crs.datum= "Geocentric_Datum_of_Australia_1994"
    crs.longitude_of_prime_meridian= 0.0
    crs.inverse_flattening=298.257222101
    crs.semi_major_axis=6378137.0
    crs.semi_minor_axis=6356752.314140356
    crs._CoordinateTransformType="Projection"
    crs._CoordinateAxisTypes="GeoX GeoY"
    # Set crs globally [alternative method!]
    setattr(ncds, 'crs:name', "GDA94")
    setattr(ncds, 'crs:datum', "Geocentric_Datum_of_Australia_1994")
    setattr(ncds, 'crs:longitude_of_prime_meridian', 0.0)
    setattr(ncds, 'crs:inverse_flattening', 298.257222101)
    setattr(ncds, 'crs:semi_major_axis', 6378137.0)
    setattr(ncds, 'crs:semi_minor_axis', 6356752.314140356)
    setattr(ncds, 'crs:_CoordinateTransformType', "Projection")
    setattr(ncds, 'crs:_CoordinateAxisTypes', "GeoX GeoY")
    # Close the file
    ncds.close()
    # Report back
    print 'Congratulations, your netCDF file is baked! See:', outfile

# Set ncfile header
x, y = (lon1, lat1) 
head = {'samples': len(x), \
  'lines': len(y), \
  'bands': 1, \
  'latsmin': min(lat1), \
  'lonsmin':min(lon1), \
  'latsmax': max(lat1), \
  'lonsmax':max(lon1), \
  'epsg':4326, \
  'resolution':0.01}

# Mean - Out
# Run the make netCDF function
make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/emast_montimeseries_tmax_meanv1.nc', varname='air_temperature', data=tmax_mean, lati=lat1, loni=lon1, header=head)


# Standard deviation - Out
# Run the make netCDF function for +pos std dev
for i in range(0,504):
    for j in range(1,41):
        make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/full_ts/emast_montimeseries_tmax_stdv1_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)+'.nc', 
                varname='air_temperature', 
                data=vars()['tmax_std_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)

# Run the make netCDF function for -neg std dev
for i in range(0,504):
    for j in range(40,80):
        make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/full_ts/emast_montimeseries_tmax_stdv1_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)+'.nc', 
                varname='air_temperature', 
                data=vars()['tmax_std_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)

# Variance - Out
# Run the make netCDF function for +pos variance
for i in range(0,504):
    for j in range(1,41):
        make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/full_ts/emast_montimeseries_tmax_varv1_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)+'.nc', 
                varname='air_temperature', 
                data=vars()['tmax_var_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)

# Run the make netCDF function for -neg standard error of the mean
for i in range(0,504):
    for j in range(40,80):
        make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/full_ts/emast_montimeseries_tmax_varv1_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)+'.nc', 
                varname='air_temperature', 
                data=vars()['tmax_var_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)


# Standard error of the mean - Out
# Run the make netCDF function for +pos standard error of the mean
for i in range(0,504):
    for j in range(1,41):
        make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/full_ts/emast_montimeseries_tmax_semv1_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)+'.nc', 
                varname='air_temperature', 
                data=vars()['tmax_sem_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j-1).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)

# Run the make netCDF function for -neg standard error of the mean
for i in range(0,504):
    for j in range(40,80):
        make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/full_ts/emast_montimeseries_tmax_semv1_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)+'.nc', 
                varname='air_temperature', 
                data=vars()['tmax_sem_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)


# Random perturbations - Out
# Run the make netCDF function for random perturbations
for i in range(0,504):
    for j in range(1,81):
        make_nc(outfile='/g/data1/rr9/IM_PhD/data/tmax_ensemble/full_ts/emast_montimeseries_tmax_ranv1_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)+'.nc', 
                varname='air_temperature', 
                data=vars()['tmax_ran_'+str(((i+1)/12)+1970)+str((i+1)%12).zfill(2)+'_ens'+str(j).zfill(2)], 
                lati=lat1, 
                loni=lon1, 
                header=head)
