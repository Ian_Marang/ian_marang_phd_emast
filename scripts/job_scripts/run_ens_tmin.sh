#!/bin/bash
#PBS -M ian.marang\@students.mq.edu.au
#PBS -P xa5
#PBS -q normal
#PBS -l walltime=03:00:00
#PBS -l mem=124gb
#PBS -l ncpus=16
#PBS -lother=gdata1
cd /home/576/ijm576/job_scripts/
python tmin_master_ens_inc40_full.py
