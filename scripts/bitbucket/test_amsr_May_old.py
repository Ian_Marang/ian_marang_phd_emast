# Finding normalised anomalies for each day in May across AMSR dataset
#Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
import os, sys
import os.path
#from osgeo import gdal, osr
from scipy import stats
from fnmatch import fnmatch

# Set nodata value
nodata = -9999.

# Define function for reading ncfile
def read_ncdata(infile=None, varname=None):
    """ Returns data from netcdf file as outdata for use in other functions
    Requires an infile = filename and a varname = variable """
    g = Dataset(infile,'r')
    outdata = np.array(g.variables[varname])
    outdata[outdata == nodata] = decimal.Decimal("NaN")
    return outdata;

os.chdir('/g/data1/rr9/Terrestrial_Ecosystems/Climate/eMAST/AMSR_E/0_05deg/v1m0_aus/day/land/amsm/e_01/2002_2011/')

# Loop through files, append to array
for i in range(1,32):    
    vars()['itr'+str(i).zfill(2)] = 0
    for root, dirs, files in os.walk(".",topdown=True):
        for name in files:
            if fnmatch(name,'*????05'+str(i).zfill(2)+'.nc'):
                if vars()['itr'+str(i).zfill(2)] ==0:
                    vars()['sm'+str(i).zfill(2)+'_data'] = read_ncdata(name,'lwe_thickness_of_soil_moisture_content')
                else:
                    new_data = read_ncdata(name,'lwe_thickness_of_soil_moisture_content')
                    vars()['sm'+str(i).zfill(2)+'_data'] = np.vstack((vars()['sm'+str(i).zfill(2)+'_data'],new_data))
                vars()['itr'+str(i).zfill(2)]=vars()['itr'+str(i).zfill(2)]+1

# Mean - In
for i in range(1,32):
    perturbm = np.mean(vars()['sm'+str(i).zfill(2)+'_data'], axis=0)
    vars()['sm'+str(i).zfill(2)+'_mean'] = perturbm

# Standard Deviation - In
for i in range(1,32):
    perturbs = np.nanstd(vars()['sm'+str(i).zfill(2)+'_data'], ddof=1, axis=0)
    vars()['sm'+str(i).zfill(2)+'_std'] = perturbs

# Generate 31 normalised anomalies for timeseries 2003-2011
for i in range(1,32):
    perturb = (np.subtract(vars()['sm'+str(i).zfill(2)+'_mean'],vars()['sm'+str(i).zfill(2)+'_data']))/vars()['sm'+str(i).zfill(2)+'_std']
    vars()['sm'+str(i).zfill(2)+'_norm_anomaly'] = perturb

# Get lats and lons from sample file
a = Dataset('/g/data1/rr9/Terrestrial_Ecosystems/Climate/eMAST/AMSR_E/0_05deg/v1m0_aus/day/land/amsm/e_01/2002_2011/eMAST_AMSRe_day_amsm_v1m0_20020620.nc')
lat1 = a.variables['latitude'][:]
lon1 = a.variables['longitude'][:]

# Function to write an eMAST formatted netCDF file - example usage below...

def make_nc(outfile=None, varname=None, data=None, lati=None, loni=None, header=None, nodata=-9999.):
    ncds = Dataset(outfile, 'w', format='NETCDF4_CLASSIC', zlib=True)
    time  = ncds.createDimension('time', None)
    lat   = ncds.createDimension('latitude', len(lati))
    lon   = ncds.createDimension('longitude', len(loni))
    times = ncds.createVariable('time','f8',('time',)) 
    latitudes = ncds.createVariable('latitude','f4',('latitude',))
    longitudes = ncds.createVariable('longitude','f4',('longitude',))
    variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',), fill_value=nodata) 
    #variable = ncds.createVariable(varname,'f4',('latitude','longitude',), fill_value=nodata) 
    latitudes[:] = lati[:]
    longitudes[:] = loni[:]
    variable[:,:,:] = data # time version of the data insert 
    #variable[:,:] = data 
    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    # Removed time variable - add as required
    times.long_name = 'time' 
    times.standard_name = 'time' 
    times.units = 'day' 
    times.calendar = 'gregorian' 
    times.axis = 'T' 
    # Set geospatial global methods [basic method for setting global attr.]
    ncds.geospatial_lat_min = header['latsmin']
    ncds.geospatial_lat_max = header['latsmax']
    ncds.geospatial_lat_units = 'degrees_north'
    ncds.geospatial_lat_resolution = header['resolution']
    ncds.geospatial_lon_min = header['lonsmin']
    ncds.geospatial_lon_max = header['lonsmax']
    ncds.geospatial_lon_units = 'degrees_east'
    ncds.geospatial_lon_resolution = header['resolution']
    crs=ncds.createVariable('crs','f4')                                      
    # Set crs as variable
    crs.name="GDA94"
    crs.datum= "Geocentric_Datum_of_Australia_1994"
    crs.longitude_of_prime_meridian= 0.0
    crs.inverse_flattening=298.257222101
    crs.semi_major_axis=6378137.0
    crs.semi_minor_axis=6356752.314140356
    crs._CoordinateTransformType="Projection"
    crs._CoordinateAxisTypes="GeoX GeoY"
    # Set crs globally [alternative method!]
    setattr(ncds, 'crs:name', "GDA94")
    setattr(ncds, 'crs:datum', "Geocentric_Datum_of_Australia_1994")
    setattr(ncds, 'crs:longitude_of_prime_meridian', 0.0)
    setattr(ncds, 'crs:inverse_flattening', 298.257222101)
    setattr(ncds, 'crs:semi_major_axis', 6378137.0)
    setattr(ncds, 'crs:semi_minor_axis', 6356752.314140356)
    setattr(ncds, 'crs:_CoordinateTransformType', "Projection")
    setattr(ncds, 'crs:_CoordinateAxisTypes', "GeoX GeoY")
    # Close the file
    ncds.close()
    # Report back
    print 'Congratulations, your netCDF file is baked! See:', outfile

# Set ncfile header
x, y = (lon1, lat1) 
head = {'samples': len(x), \
  'lines': len(y), \
  'bands': 1, \
  'latsmin': min(lat1), \
  'lonsmin':min(lon1), \
  'latsmax': max(lat1), \
  'lonsmax':max(lon1), \
  'epsg':4326, \
  'resolution':0.05}


# Run the make netCDF function for means
for i in range(1,32):
    make_nc(outfile='/g/data1/rr9/IM_PhD/data/AMSR_ensemble/emast_amsr_mean_2002-2011_May_'+str(i).zfill(2)+'.nc', 
            varname='mean_lwe_thickness_of_soil_moisture_content', 
            data=vars()['sm'+str(i).zfill(2)+'_mean'], 
            lati=lat1, 
            loni=lon1, 
            header=head)


# Run the make netCDF function for anomalies
for i in range(1,32):
    make_nc(outfile='/g/data1/rr9/IM_PhD/data/AMSR_ensemble/emast_amsr_anomaly_2002-2011_May_'+str(i).zfill(2)+'.nc', 
            varname='anomaly_lwe_thickness_of_soil_moisture_content', 
            data=vars()['sm'+str(i).zfill(2)+'_norm_anomaly'], 
            lati=lat1, 
            loni=lon1, 
            header=head)
