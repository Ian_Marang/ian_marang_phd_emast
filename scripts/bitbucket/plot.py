import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
from netCDF4 import Dataset
import os
from matplotlib import animation
import pandas as pd
from matplotlib.colors import LinearSegmentedColormap
from  matplotlib import colors as colors
from matplotlib._png import read_png
import eMAST.path_maker as pm
import eMAST.metadata as md
import eMAST.colorconvention as cc
import matplotlib.image as mpimg
from mpl_toolkits.basemap import maskoceans
import sys
#import PIL
#import Image
#from PIL import Image,ImageDraw, ImageFilter, ImageFont
#import _imaging
colorBarGlobal = None
class Map:
	def __init__(self,variable,latitude,longitude,varName,mapType,day,month,year, outputDirectory, outputFileName):
	
               
		self.mapType = mapType	
	        self.metaData=md.MetaData(varName)	
		#self.variableArray = netCDFObj.variables['layer'][:]
		self.variableArray=variable
		self.latArray = latitude
		self.lonArray = longitude	
		#remove any infinite values as this destroys the color pallete
		print('removing infinite')
		self.variableArray[self.variableArray==np.NINF]=float('nan')
		self.variableArray[self.variableArray==np.PINF]=float('nan')
		print(np.max(self.variableArray))
		print(np.min(self.variableArray))
			
		print(self.variableArray.shape)
		print(self.variableArray)
		
		#print(self.metaData.getChartUnits())
		
		self.varName=varName
		self.day=pm.fixZero(day)
		self.month=pm.fixZero(month)
		self.year=pm.fixDate(year)
		self.outputDirectory=outputDirectory
		self.outputFileName = outputFileName
		self.extentChange = 0
		self.latMinExtent=np.amin(self.latArray)+self.extentChange
		self.latMaxExtent=np.amax(self.latArray)+self.extentChange
		self.lonMinExtent=np.amin(self.lonArray)-self.extentChange
		self.lonMaxExtent=np.amax(self.lonArray)+self.extentChange
		self.plot=""
		self.layer=""
		self.colorMap=""
		self.title=""
                self.date=""
		self.metaData=md.MetaData(varName)
		self.colorConvention =cc.ColorConvention('/g/data/rr9/sjj576/scripts/python/eMAST/csv/colorConvention.csv',self.varName)

	
	def plotMap(self):			
		# get color map
		self.colorMap=plt.get_cmap(self.colorConvention.colorMap)
		
		# cut of antartica if required
		if self.colorConvention.cutLat=='yes':
			self.latMinExtent=-60
		
		#initial basemap object
		self.plot= Basemap(projection='cyl',llcrnrlat=self.latMinExtent,urcrnrlat=self.latMaxExtent,llcrnrlon=self.lonMinExtent,urcrnrlon=self.lonMaxExtent,resolution='f')
		

		# read australian shape file
		if self.latMinExtent>-46-self.extentChange:
			self.plot.readshapefile('/g/data/rr9/sjj576/scripts/python/eMAST/shapeFiles/australia', 'states')	
		
		
		self.plot.drawmapboundary(fill_color='white')
		lon, lat = np.meshgrid(self.lonArray, self.latArray)
		xi, yi = self.plot(lon, lat)
		
		mask=maskoceans(lon, lat,self.variableArray)

		#get levels for map
		levels = self.colorConvention.buckets.split("|")
		print(levels)
                levels = [float(i) for i in levels]

		# set different contourf attributes for large map
		if self.mapType == 'pngLarge':
			self.layer=self.plot.contourf(xi,yi,self.variableArray,cmap=self.colorMap,vmin=-self.colorConvention.minValue,vmax=self.colorConvention.maxValue,alpha=0.6,antialiased=True,levels=levels)
		
		# set different contourf attributes for medium map
		if self.mapType == 'pngMedium':
			mask=maskoceans(lon, lat,self.variableArray)
			self.layer= self.plot.contourf(xi,yi,mask,levels=levels,cmap=self.colorMap,vmin=-self.colorConvention.minValue,vmax=self.colorConvention.maxValue)	
		
		# set different attributes with small map
		if self.mapType == 'pngSmall':
			mask=maskoceans(lon, lat,self.variableArray)
			self.layer= self.plot.contourf(xi,yi,mask,levels=levels,cmap=self.colorMap,vmin=-self.colorConvention.minValue,vmax=self.colorConvention.maxValue)

	
	def writePNGLarge(self, customMetaData):
		
                print('Printing PNG Large')
              	
		# obtain background pngs from server
		self.plot.arcgisimage(server='http://server.arcgisonline.com/ArcGIS',service='ESRI_Imagery_World_2D', xpixels = 1500, verbose= True)

		#set text values to go on map
		date = str(self.day)+' - '+str(self.month)+' - '+str(self.year)
                longName=None
		if customMetaData==None:
			longName='%s %s'%(self.metaData.getLongName(),self.metaData.getChartUnits())
		else:
			longName ='%s %s' %(customMetaData[0], customMetaData[1])
		url='http://www.emast.org.au'	
		
		# determine locations for text on map from csv file
		print(self.colorConvention.lURLLoc)
		urlLoc = self.colorConvention.lURLLoc.split("|")
		urlLoc = [float(i) for i in urlLoc]
		
		print(self.colorConvention.lNameLoc)
		longNameLoc=self.colorConvention.lNameLoc.split("|")
		longNameLoc = [float(i) for i in longNameLoc]
               	
                
		dateLoc=self.colorConvention.lDateLoc.split("|")
                dateLoc = [float(i) for i in dateLoc]
		
		#determine if csv loc needs to be adjusted becuase of cutting anatartica
		if self.colorConvention.cutLat=='yes':
			offset=30
			longNameLoc[1]=longNameLoc[1]+offset
			dateLoc[1]=dateLoc[1]+offset
		
		#plot text based on locations
		if self.colorConvention.removeText!='yes':
			plt.text(urlLoc[0],urlLoc[1],url,fontsize=16,color='white')
               		plt.text(longNameLoc[0],longNameLoc[1],longName,fontsize=16,color='white',weight='bold')
			plt.text(dateLoc[0],dateLoc[1],date,fontsize=17,color='white',weight='bold')

		# set color bar
		if self.colorConvention.removeBar!='yes':
			colorBar=self.plot.colorbar(self.layer, location='bottom')
             
			t = colorBar.ax.get_xticklabels();
			labels = [item.get_text() for item in t]
		
			colorBar.ax.set_xticklabels(labels, color = 'white')
			colorBar.ax.xaxis.set_label_coords(0.12,-0.75)

			colorBar.draw_all()
		
		#set size
		fig = matplotlib.pyplot.gcf()
                fig.set_size_inches(14,10)
		#fig.set_size_inches(24,16)
	
		#write the plot to file
		outputPath=self.outputDirectory+'pngLarge/'+self.outputFileName+'.png'

                if not os.path.exists(self.outputDirectory+'/pngLarge/'):
                       os.makedirs(self.outputDirectory+'/pngLarge')
                
		plt.savefig(outputPath,bbox_inches='tight',pad_inches=0.2,facecolor='black')
		plt.close()
		
	
	def writePNGMedium(self):
                # create output path for png medium
		print('Printing PNG Medium')
                
		#get eMAST Logo
		logo = read_png('/g/data/rr9/sjj576/scripts/python/eMAST/images/example.png')
                
               	
		#set color bar
		colorBar=self.plot.colorbar(self.layer, location='right')
	        colorBar.set_clim(self.colorConvention.minValue,self.colorConvention.maxValue)	
		colorBar.draw_all()
		
		#set text values
 		date = str(self.day)+'-'+str(self.month)+'-'+str(self.year)
                longName=self.metaData.getLongName()
                units=self.metaData.getChartUnits()
		url = 'http://www.emast.org.au'
                title = '%s%s\n%s' %(longName,units,date)
               
		#remove axis
		plt.gca().axison = False
		
		#get text location from csv
		textLoc = self.colorConvention.mTextLoc.split("|")
		textLoc = [float(i) for i in textLoc]
		
		# get URL loc from csv
		urlLoc = self.colorConvention.mURLLoc.split("|")
                urlLoc = [float(i) for i in urlLoc]

		#write text onto plot
		plt.text(urlLoc[0],urlLoc[1],url,fontsize=8)
		plt.text(textLoc[0],textLoc[1],title,fontsize=10)

		# get logo offset from bottom left (in pixels, not lats and lons)
		logoLoc = self.colorConvention.mLogoLoc.split('|')
		logoLoc = [int(i) for i in logoLoc]		

		# print logo to map
		plt.figimage(logo,zorder=10,xo = logoLoc[0], yo=logoLoc[1])
                
		#set size
		fig = matplotlib.pyplot.gcf()
                fig.set_size_inches(7.29,4.6875)
 		
		#draw coastlines if configured to
		if self.colorConvention.coastlines=='yes':
			self.plot.drawcoastlines(linewidth=0.25)			
		
		# draw land see mask to help hide missing data
		self.plot.drawlsmask()
				
		# create directory and save to file
		outputPath=self.outputDirectory+'pngMedium/'+self.outputFileName+'.png'
		
		if not os.path.exists(self.outputDirectory+'/pngMedium/'):
                       os.makedirs(self.outputDirectory+'/pngMedium')
                
		plt.savefig(outputPath,pad_inches=0.25,bbox_inches='tight')
		
		plt.close()
		

	def writePDF(self):
		outputPath=self.outputDirectory+'pdf/'+self.outputFileName+'.pdf'
		print('Printing PDF')
                date = str(self.day)+'/'+str(self.month)+'/'+str(self.year)
                longName=self.metaData.getLongName()
                title = longName+'\n'+date
                units=self.metaData.getUnits()
                plt.title(title)
              
		colorBar=self.plot.colorbar(self.layer, location='bottom')
                colorBar.set_label(units)
                #plt.figure(frameon=True)
                if not os.path.exists(self.outputDirectory+'/pdf/'):
                       os.makedirs(self.outputDirectory+'/pdf')
                plt.savefig(outputPath,format='pdf')
	
	def writePNGSmall(self):	
		print('Printing Animation') 
		
		# get text location and write text to plot
		textLoc = self.colorConvention.sTextLoc.split("|")
                textLoc = [float(i) for i in textLoc]
		plt.text(textLoc[0],textLoc[1],self.year,fontsize=10)
		
		# turn of axis
		plt.gca().axison = False
		
		#set size
		fig = matplotlib.pyplot.gcf()
		fig.set_size_inches(1.4,1.4)
		
		# create path and save to output
		outputPath=self.outputDirectory+'/pngSmall/'+self.outputFileName+'.png'
		if not os.path.exists(self.outputDirectory+'/pngSmall/'):
                       os.makedirs(self.outputDirectory+'/pngSmall')
		plt.savefig(outputPath, bbox_inches='tight',pad_inches=0.025,figsize=(1.4,1.4))
		
		plt.close()



	def writePNGSmallAnnualAverage(self):
                # obtain land mask from elevation

                print('Printing Animation')

                # get text location and write text to plot
                textLoc = self.colorConvention.sTextLoc.split("|")
                textLoc = [float(i) for i in textLoc]
                plt.text(textLoc[0],textLoc[1],self.year,fontsize=10)

                # turn of axis
                plt.gca().axison = False

                #set size
                fig = matplotlib.pyplot.gcf()
                fig.set_size_inches(1.5,1.5)

                # create path and save to output
                outputPath=self.outputDirectory+'/pngSmallAnnualAverage/'+self.outputFileName+'.png'
                if not os.path.exists(self.outputDirectory+'/pngSmallAnnualAverage/'):
                       os.makedirs(self.outputDirectory+'/pngSmallAnnualAverage')
                plt.savefig(outputPath, bbox_inches='tight',pad_inches=0.025,figsize=(1.4,1.4))

                plt.close()

	
class Writer:
	def __init__(self,startYear,endYear,varName, specificMonth=None):
		self.startYear=startYear
		self.endYear=endYear
		self.varName=varName
	        self.filePath =pm.FilePath(self.varName)
		self.specificMonth =specificMonth
		self.variableArray=None
		self.latArray=None
		self.longArray=None
		self.variable3d=None

	def write(self,imageType,annualAverage=True,customMetaData=None):
		
		# set up years /months to process
		years = range(self.startYear,self.endYear)
		month = None
		if self.filePath.frequency=='mon':
			months = range(1,13)

		if self.filePath.frequency=='yr':
			months = range(1,2)

	 	day =1	
		 	
		for year in years:
			for month in months:
			  	print(month)		
				print(self.specificMonth)
				
				# if specifc month not set, then will go through every month
				if self.specificMonth is None:
					print(self.specificMonth)
					self.specificMonth=month
						
				#if specfic month is set, the go through only 1 month
				if self.specificMonth==month:	
					

					print(self.specificMonth)
					# get input path
					inputPath=self.filePath.getInputDirectoryStructure(year,month,day)
					print('Input Path: '+inputPath)
					
					#get output directory and fileanme
					outputDirectoryStructure=self.filePath.getVisualisationDirectoryStructure()
					outputFileName = self.filePath.getOutputFileName(year,month,day)	
					print('Output directory Structure: '+outputDirectoryStructure)
					print('Output file name: '+outputFileName)
				       
					netCDFObj = Dataset(inputPath)
					print(netCDFObj)
							
					# get data, lat and long from netcdfFile
				        self.variableArray = netCDFObj.variables['layer'][:]
					self.latArray = netCDFObj.variables['latitude'][:]
        			        self.lonArray =  netCDFObj.variables['longitude'][:]
	
					
					#close net cdf
                                        netCDFObj.close()
	
									
					if annualAverage == True:
						print('Obtaining month %s') % (month)
						if month == 1:
							self.variable3d = self.variableArray
							print(self.variable3d.shape)
							#sys.exit()
						if month>1:
							print('Stacking %s') % (year)
							self.variable3d = np.dstack((self.variable3d, self.variableArray))
							print(self.variable3d)
							print(self.variable3d.shape)			
						if month ==12: 
							self.variableArray = np.mean(self.variable3d,axis=2)
							print(self.variableArray)
							print(self.variableArray.shape)
							#sys.exit()	
					if annualAverage == False or month == 12:
						# create map object
						map = Map(self.variableArray,self.latArray,self.lonArray,self.varName,imageType,day,month,year,outputDirectoryStructure,outputFileName)
						
						#plot ma
						map.plotMap()
						#sys.exit()	
						# write output based on plotType
						if imageType == 'pngMedium':
							print('Writing PNG')
							map.writePNGMedium()
							
						if imageType == 'pdf':
							print ('Writing  PDF')
							map.writePDF()
						
						if imageType =='pngSmall':
							print('Printing Animations')
							if annualAverage == False:
								map.writePNGSmall()
							if annualAverage == True:
								map.writePNGSmallAnnualAverage()
						
						
						if imageType =='pngLarge':
							print('Printing Large PNG')
							map.writePNGLarge(customMetaData)
						
						# reset specifc month so can iterate all months
					self.specificMonth=None
# create writer object
writer = Writer(2012,2013,'eMAST_eWATCH_mon_alph_v1m1_1979_2012',None)

writer.write('pngLarge',annualAverage=False)


#eMAST_mon_tmax_
#eMAST_mon_parr_v1m1_1970_2012
#eMAST_mon_etpo_v1m1_1970_2012
#eMAST_mon_runo_v1m1_1970_2012
#eMAST_mon_gd10_v1m1_1970_2012
#eMAST_mon_gd05_v1m1_1970_2012
#eMAST_mon_gd00_v1m1_1970_2012
#eMAST_mon_eteq_v1m1_1970_2012
#eMAST_mon_etde_v1m1_1970_2012
#eMAST_mon_moin_v1m1_1970_2012
#eMAST_mon_chil_v1m1_1970_2012
#eMAST_mon_etac_v1m1_1970_2012
#eMAST_mon_parr_v1m1_1970_2012
