# Final working ensemble generator - Precipitation - Full Time Series 1970-2012
# Uses os.walk to find file location
# Gives standard error of the mean with +/- 20 increments for each month
# IN for loops nested to provide: (for pos inc) i) data step, j) ens index start at 00
#                                 (for neg inc) i) data step, j) neg inc, k) ens index start at 00
# OUT for loops nested to provide: (for both inc) i) data step, j) ens index start at 00 


#Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
import os, sys
import os.path
from scipy import stats
import eMAST.path_maker as pm
import pandas as pd
import eMAST.metadata as md
from numpy import genfromtxt

def process(productsInputs,productsOutputs,yearList,monthList):
	for productInput in productsInputs:
		print('Starting processing for %s') %(productInput)
		filePath = pm.FilePath(productInput)
		metaData = md.MetaData(productInput)
		data3d = get3dArray(productInput,yearList,monthList,filePath,metaData)
		totalStatistics ={}
		totalStatistics['mean'] = get3dArrayMean(data3d)
		totalStatistics['standardDeviation']=get3dArrayStandardDeviation(data3d)
		totalStatistics['variance'] = get3dArrayVariance(data3d)
		totalStatistics['standardError'] = get3dArrayStandardError(data3d)
		netCDFLatsLons=""	
		for year in yearList:
			print('Starting processing for input: %s year: %s') %(productInput, year)
			
			# this variable is for counting the month e.g. 1 to 504
			monthCounter =0
			for month in monthList:
				print('Starting processing for product: %s year: %s month: %s') %(productInput,year,month)
				
				print('Getting longitude and latitude for all files')
			 	inputFile = filePath.getInputDirectoryStructure(str(year),pm.fixZero(month),1)
				
				#open netcdf file
				netCDFObj = Dataset(inputFile,'r')
				
				if monthCounter==0:
					netCDFLatsLons=netCDFObj
				for productOutput in productsOutputs:
					print(productOutput)
					print('Starting processing for product %s year: %s month: %s output : %s') %(productInput,year,month,productOutput)
					if productOutput == 'standardDeviation':
						print('Getting standard Deviation Ensemble')
						generateStdDev(data3d, totalStatistics['standardDeviation'], 'positive',month,year,filePath,metaData,netCDFObj)
						generateStdDev(data3d, totalStatistics['standardDeviation'], 'negative',month,year,filePath,metaData,netCDFObj)
						
					if productOutput == 'variance':
						print('Getting variance ensemble')
						generateVariance(data3d, totalStatistics['variance'],'positive',month,year,filePath,metaData,netCDFObj)
						generateVariance(data3d, totalStatistics['variance'],'negative',month,year,filePath,metaData,netCDFObj)
					if productOutput=='standardError':
						print('Getting Standard Error ensemble')	
						generateStandardError(data3d, totalStatistics['standardError'],'positive',month,year,filePath,metaData,netCDFObj)
						generateStandardError(data3d, totalStatistics['standardError'],'negative',month,year,filePath,metaData,netCDFObj)
					
					if productOutput=='random':
						print('Generating Random ensemble')
						generateRandom2(data3d,month,year,filePath,metaData,netCDFObj)
					if productOutput=='anomolies':
						print('Generating Anomolies')
						generateAnomolies(totalStatistics['mean'],totalStatistics['standardDeviation'], data3d[monthCounter,:,:],month,year,filePath,metaData,netCDFObj)						          
					# iterate month counter
				monthCounter = monthCounter +1
		print('Saving Total Statistics')
		print(netCDFLatsLons)	
		saveTotalStatisticsNetCDF(totalStatistics,productInput,filePath,metaData,netCDFLatsLons)
		

# Define function for reading ncfile
def read_ncdata(infile=None, varname=None):
    """ Returns data from netcdf file as outdata for use in other functions
    Requires an infile = filename and a varname = variable """
    g = Dataset(infile,'r')
    outdata = np.array(g.variables[varname])
    outdata[outdata == nodata] = decimal.Decimal("NaN")
    return outdata;


def createRandomIndexCSV(rows,mu,sigma):
	#mu, sigma = 0, 10e-11
        
	s = np.random.normal(mu, sigma, 80)
 	print(s)
	print('Saving CSV')
	np.savetxt("eMAST/csv/random.csv", s, delimiter=",")	
	print('Saving Complete')
	print(s.shape)
	print(s.shape[0])
	print(s[0])

def get3dArray(product,yearList,monthList,filePath,metaData):	
	# Define Iterator count for array creation during loop through files
	itr=0
	print(yearList)
	print(monthList)
	varName = metaData.getStandardName()
	# Loop through files, append to array
	for year in yearList:
	    for month in monthList:
		name = filePath.getInputDirectoryStructure(str(year),pm.fixZero(month),1)
		print(name)
		if itr==0:
			data = read_ncdata(name,varName)
			print(data.shape)
		
		else:
			new_data = read_ncdata(name,varName)
			data = np.vstack((data,new_data))
			print(data.shape)
		itr=itr+1
	return data


def saveTotalStatisticsNetCDF(totalStatistics,productInput,filePath,metaData,netCDFObj):
        
        lat1 = np.array(netCDFObj.variables['latitude'])
        lon1 = np.array(netCDFObj.variables['longitude'])
        
	for totalStatisticKey,statisticArray in totalStatistics.iteritems():
		
		initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s_%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm,filePath.variable,totalStatisticKey)


        	completeDirectoryPath='%s/%s/%s'%(initialDirectory,filePath.ensemble,filePath.timeSeries)

        	fileName = '%s_%s_%s_%s_%s.nc'% (filePath.model,filePath.frequency,filePath.variable,totalStatisticKey,filePath.version)
        	completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
        	pm.checkAndCreatePath(completeDirectoryPath)

        	print('Writing Output for Total %s in %s: ')%(totalStatisticKey,completeFileName)
        	make_nc(outfile=completeFileName,
                	varname=filePath.variable,
                	data=statisticArray,
                	lati=lat1,
                	loni=lon1,
                	header=getHeader(lon1,lat1))
        	print('Finished Writing Output for Total %s in %s: ')%(totalStatisticKey,completeFileName)


def get3dArrayMean(data):
	# Mean - In
	# Find Mean across months (ignoring NaN)
	mean = np.mean(data, axis=0)
	print(mean.shape)
	print(mean)
	return mean

def get3dArrayStandardDeviation(data):
	# Standard Deviation - In
	# Find standard deviation increment(ignoring NaN)
	std_inc =(np.nanstd(data, ddof=1, axis=0))
	print(std_inc.shape)
	print(std_inc)
	return(std_inc)

def get3dArrayVariance(data):
	# Find variance increment
	var_inc = (np.nanvar(data, ddof=1, axis=0))/20
	print(var_inc.shape)
	print(var_inc)
	return(var_inc)
	
def get3dArrayStandardError(data):
	sem_inc = (stats.sem(data, axis=0))/20
	print(sem_inc.shape)
	print(sem_inc)
	return(sem_inc)
	
def generateAnomolies(mean,standardDeviation, data, month,year,filePath,metaData,netCDFObj):
	lat1 = np.array(netCDFObj.variables['latitude']) 
        lon1 = np.array(netCDFObj.variables['longitude'])
        varName = metaData.getStandardName()
	yearString= str(year)
        monthString=pm.fixZero(month)
        ensembleType = 'anomolies'
	initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s_%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm,filePath.variable,ensembleType)
        
        anomolies = (data[:]-(mean))/standardDeviation	
       	print(anomolies.shape)
	print(anomolies)
       
	completeDirectoryPath='%s/%s/%s'%(initialDirectory,filePath.ensemble,filePath.timeSeries)
	fileName = '%s_%s_%s_%s_%s_%s%s_anom.nc'% (filePath.institution,
					filePath.model,filePath.frequency,filePath.variable,
					filePath.version,year,pm.fixZero(month))
	completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
	pm.checkAndCreatePath(completeDirectoryPath)

	print('Writing Output for %s: ')%(completeFileName)
	make_nc(outfile=completeFileName,
		varname=varName,
		data=anomolies,
		lati=lat1,
		loni=lon1,
		header=getHeader(lon1,lat1))
	print('Finished Writing Output for %s: ')%(completeFileName)

# Generate products for each month + 40 standard deviation increments
def generateStdDev(data, std_inc,direction,month,year,filePath,metaData,netCDFObj):
	lat1 = np.array(netCDFObj.variables['latitude']) 
	lon1 = np.array(netCDFObj.variables['longitude'])
	varName = metaData.getStandardName()
	yearString= str(year)
	monthString=path_maker.fixZero(month)
	ensembleType = 'standard_deviation'
	initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s'%(filePath.field,
				filePath.activity,filePath.institution,	
				filePath.model,	filePath.resolution,	
				filePath.version,filePath.frequency,  	
				filePath.realm,	filePath.variable,ensembleType)
        peturbs = range(1,41)	
	perturb=""
	for j in peturbs:
		print(j)
		print('Peturb. : '+str(j))
		ensemble =""	
		if direction =='positive':
			print('Getting positive standard deviations')	
			perturb = data[month-1,:,:]+(std_inc*(j))
			print(data.shape)
			print(data)
			ensemble = j
		
		if direction == 'negative':
           	       	print('Getting 40 negative standard deviations')
                        k = j+40
                        perturb = (data[month-1,:,:]+(std_inc*(-1*j)))
			#stDev['std_'+yearString+monthString+'_ens'+str(k)]= perturb1
               		ensemble = k
	      	completeDirectoryPath='%s/e_%s/%s'%(initialDirectory,path_maker.fixZero(ensemble),filePath.timeSeries)
                fileName = '%s_%s_%s_%s_%s_%s%s_e%s.nc'% (filePath.institution,
                                                filePath.model,filePath.frequency,filePath.variable,
                                                filePath.version,year,path_maker.fixZero(month),path_maker.fixZero(ensemble))
		completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
		path_maker.checkAndCreatePath(completeDirectoryPath)
		print(perturb.shape)
		print(perturb)
		
		print('Writing Output for %s: ')%(completeFileName)
		make_nc(outfile=completeFileName,
                        varname=varName,
                        data=perturb,
                        lati=lat1,
                        loni=lon1,
                        header=getHeader(lon1,lat1))
		print('Finished Writing Output for %s: ')%(completeFileName)
		

def generateVariance(data, variance,direction,month,year,filePath,metaData,netCDFObj):
        lat1 = np.array(netCDFObj.variables['latitude'])
        lon1 = np.array(netCDFObj.variables['longitude'])
        varName = metaData.getStandardName()
        yearString= str(year)
        monthString=path_maker.fixZero(month)
        ensembleType = 'variance'
        initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm, filePath.variable,ensembleType)

        peturbs = range(1,41)
        perturb=""
	for j in peturbs:
                print(j)
                print('Peturb. : '+str(j))
                ensemble =""
                if direction =='positive':
                        print('Getting 40 positive variance pertubations')
                        perturb = data[month-1,:,:]+(variance*(j))
                        print(data.shape)
                        print(data)
                        ensemble = j

                if direction == 'negative':
                        print('Getting 40 negative variance pertubations')
                        k = j+40
                        perturb = (data[month-1,:,:]+(variance*(-1*j)))
                      
                        ensemble = k
                completeDirectoryPath='%s/e_%s/%s'%(initialDirectory,path_maker.fixZero(ensemble),filePath.timeSeries)
                fileName = '%s_%s_%s_%s_%s_%s%s_e%s.nc'% (filePath.institution,
                                                filePath.model,filePath.frequency,filePath.variable,
                                                filePath.version,year,path_maker.fixZero(month),path_maker.fixZero(ensemble))
                completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
                path_maker.checkAndCreatePath(completeDirectoryPath)
                print(perturb.shape)
                print(perturb)

                print('Writing Output for %s: ')%(completeFileName)
                make_nc(outfile=completeFileName,
                        varname=varName,
                        data=perturb,
                        lati=lat1,
                        loni=lon1,
                        header=getHeader(lon1,lat1))
                print('Finished Writing Output for %s: ')%(completeFileName)


def generateStandardError(data, standardError,direction,month,year,filePath,metaData,netCDFObj):
        lat1 = np.array(netCDFObj.variables['latitude'])
        lon1 = np.array(netCDFObj.variables['longitude'])
        varName = metaData.getStandardName()
        yearString= str(year)
        monthString=path_maker.fixZero(month)
        ensembleType = 'standard_error'
        initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm, filePath.variable,ensembleType)

        peturbs = range(1,41)
        perturb=""
	for j in peturbs:
                print(j)
                print('Perturb. : '+str(j))
                ensemble =""
                if direction =='positive':
                        print('Getting positive standard error')
                        perturb = data[month-1,:,:]+(standardError*(j))
                        print(perturb.shape)
                        print(perturb)
                        ensemble = j

                if direction == 'negative':
                        print('Getting 40 negative standard errors')
                        k = j+40
                        perturb = (data[month-1,:,:]+(standardError*(-1*j)))
                        ensemble = k
                completeDirectoryPath='%s/e_%s/%s'%(initialDirectory,path_maker.fixZero(ensemble),filePath.timeSeries)
                fileName = '%s_%s_%s_%s_%s_%s%s_e%s.nc'% (filePath.institution,
                                                filePath.model,filePath.frequency,filePath.variable,
                                                filePath.version,year,path_maker.fixZero(month),path_maker.fixZero(ensemble))
                completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
                path_maker.checkAndCreatePath(completeDirectoryPath)
                print(perturb.shape)
                print(perturb)
                print('Writing Output for %s: ')%(completeFileName)
                make_nc(outfile=completeFileName,
                        varname=varName,
                        data=perturb,
                        lati=lat1,
                        loni=lon1,
                        header=getHeader(lon1,lat1))
                print('Finished Writing Output for %s: ')%(completeFileName)

def generateRandom(data,month,year,filePath,metaData,netCDFObj):
        lat1 = np.array(netCDFObj.variables['latitude'])
        lon1 = np.array(netCDFObj.variables['longitude'])
        varName = metaData.getStandardName()
        yearString= str(year)
        monthString=path_maker.fixZero(month)
        ensembleType = 'random'
        initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm, filePath.variable,ensembleType)

        peturb=""
	# Random perturbations - In
        # Assign mean and standard deviation for perturbation
        mu, sigma = 0, 10e-5
        s = np.random.normal(mu, sigma, 80)

	peturbs = range(1,81)
        for j in peturbs:
                print(j)
                print('Peturb. : '+str(j))
           	ensemble = j
                print('Getting Random pertubation')
                perturb = (data[month-1,:,:]+(data[month-1,:,:]*j))
                print(data.shape)
               	print(data)
                completeDirectoryPath='%s/e_%s/%s'%(initialDirectory,path_maker.fixZero(ensemble),filePath.timeSeries)
                fileName = '%s_%s_%s_%s_%s_%s%s_e%s.nc'% (filePath.institution,
                                                filePath.model,filePath.frequency,filePath.variable,
                                                filePath.version,year,path_maker.fixZero(month),path_maker.fixZero(ensemble))
                completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
                path_maker.checkAndCreatePath(completeDirectoryPath)
                print(perturb.shape)
                print(perturb)
                print('Writing Output for %s: ')%(completeFileName)
                make_nc(outfile=completeFileName,
                        varname=varName,
                        data=perturb,
                        lati=lat1,
                        loni=lon1,
                        header=getHeader(lon1,lat1))
                print('Finished Writing Output for %s: ')%(completeFileName)


def generateRandom2(data,month,year,filePath,metaData,netCDFObj):
        lat1 = np.array(netCDFObj.variables['latitude'])
        lon1 = np.array(netCDFObj.variables['longitude'])
        varName = metaData.getStandardName()
        yearString= str(year)
        monthString=pm.fixZero(month)
        ensembleType = 'random'
        initialDirectory = '/g/data1/rr9/sjj576/%s/%s/%s/%s/%s/%s/%s/%s/%s_%s'%(filePath.field,
                                filePath.activity,filePath.institution,
                                filePath.model, filePath.resolution,
                                filePath.version,filePath.frequency,
                                filePath.realm, filePath.variable,ensembleType)

        randomArray = genfromtxt('/g/data/rr9/sjj576/scripts/python/eMAST/csv/random.csv', delimiter=',')
	rows = randomArray.shape[0]
	peturb=""
        # Random perturbations - In
        # Assign mean and standard deviation for perturbation
        #mu, sigma = 0, 10e-11
        #s = np.random.normal(mu, sigma, 80)

        peturbs = range(0,rows)
        for j in peturbs:
                print(j)
                print('Peturb. : '+str(j))
                ensemble = j+1
                print('Getting Random pertubation')
                perturb = (data[month-1,:,:]+(data[month-1,:,:]*randomArray[j]))
                print(data.shape)
                print(data)
                completeDirectoryPath='%s/e_%s/%s'%(initialDirectory,pm.fixZero(ensemble),filePath.timeSeries)
                fileName = '%s_%s_%s_%s_%s_%s_e%s_%s%s.nc'% (filePath.institution,
                                                filePath.model,filePath.frequency,filePath.variable,ensembleType,
                                                filePath.version,pm.fixZero(ensemble),yearString,monthString)
                completeFileName = '%s/%s' %(completeDirectoryPath,fileName)
                pm.checkAndCreatePath(completeDirectoryPath)
                print(perturb.shape)
                print(perturb)
                print('Writing Output for %s: ')%(completeFileName)
                make_nc(outfile=completeFileName,
                        varname=varName,
                        data=perturb,
                        lati=lat1,
                        loni=lon1,
                        header=getHeader(lon1,lat1))
                print('Finished Writing Output for %s: ')%(completeFileName)



def make_nc(outfile=None, varname=None, data=None, lati=None, loni=None, header=None, nodata=-9999.):
    data[np.isnan(data)]=nodata
    ncds = Dataset(outfile, 'w', format='NETCDF4_CLASSIC', zlib=True)
    time  = ncds.createDimension('time', None)
    lat   = ncds.createDimension('latitude', len(lati))
    lon   = ncds.createDimension('longitude', len(loni))
    #t
    latitudes = ncds.createVariable('latitude','f4',('latitude',))
    longitudes = ncds.createVariable('longitude','f4',('longitude',))
    #variable = ncds.createVariable(varname,'f8',('time','latitude','longitude',), fill_value=nodata) 
    variable = ncds.createVariable(varname,'f4',('latitude','longitude',), fill_value=nodata) 
    latitudes[:] = lati[:]
    longitudes[:] = loni[:]
    #variable[:,:,:] = data # time version of the data insert 
    
    variable[:,:] = data 
    latitudes.long_name = 'latitude'
    latitudes.standard_name = 'latitude'
    latitudes.units = 'degrees_north'
    latitudes.axis = 'Y'
    longitudes.long_name = 'longitude'
    longitudes.standard_name = 'longitude'
    longitudes.units = 'degrees_east'
    longitudes.axis = 'X'
    ncds.geospatial_lat_min = header['latsmin']
    ncds.geospatial_lat_max = header['latsmax']
    ncds.geospatial_lat_units = 'degrees_north'
    ncds.geospatial_lat_resolution = header['resolution']
    ncds.geospatial_lon_min = header['lonsmin']
    ncds.geospatial_lon_max = header['lonsmax']
    ncds.geospatial_lon_units = 'degrees_east'
    ncds.geospatial_lon_resolution = header['resolution']
    crs=ncds.createVariable('crs','f4')                                      
    # Set crs as variable
    crs.name="GDA94"
    crs.datum= "Geocentric_Datum_of_Australia_1994"
    crs.longitude_of_prime_meridian= 0.0
    crs.inverse_flattening=298.257222101
    crs.semi_major_axis=6378137.0
    crs.semi_minor_axis=6356752.314140356
    crs._CoordinateTransformType="Projection"
    crs._CoordinateAxisTypes="GeoX GeoY"
    # Set crs globally [alternative method!]
    setattr(ncds, 'crs:name', "GDA94")
    setattr(ncds, 'crs:datum', "Geocentric_Datum_of_Australia_1994")
    setattr(ncds, 'crs:longitude_of_prime_meridian', 0.0)
    setattr(ncds, 'crs:inverse_flattening', 298.257222101)
    setattr(ncds, 'crs:semi_major_axis', 6378137.0)
    setattr(ncds, 'crs:semi_minor_axis', 6356752.314140356)
    setattr(ncds, 'crs:_CoordinateTransformType', "Projection")
    setattr(ncds, 'crs:_CoordinateAxisTypes', "GeoX GeoY")
    # Close the file
    ncds.close()
    # Report back
    print 'Congratulations, your netCDF file is baked! See:', outfile

def getHeader(lon1,lat1):
# Set ncfile header
    x, y = (lon1, lat1) 
    head = {'samples': len(x), \
    'lines': len(y), \
    'bands': 1, \
    'latsmin': min(lat1), \
    'lonsmin':min(lon1), \
    'latsmax': max(lat1), \
    'lonsmax':max(lon1), \
    'epsg':4326, \
    'resolution':0.01}
    return(head)


def generateRandomCSV():
	print('Generating Random CSV')




# global variables
nodata=-9999.

# Establish which products to perturb

#productsInput=['eMAST_ANUClimate_mon_tmin_v1m0_1970_2012']

productsInput = ['eMAST_ANUClimate_mon_prec_v1m0_1970_2012']

# 		'eMAST_ANUClimate_mon_rrad_v1m0_1970_2012',
#		'eMAST_ANUClimate_mon_tmax_v1m0_1970_2012',
# 		'eMAST_ANUClimate_mon_tmin_v1m0_1970_2012']

productsOutput = ['anomolies','random']
#productsOutput = ['standardDeviation','variance','standardError','random']
#productsOutput = ['variance','standardError','random']
#productsOutput = ['standardError','random']



#establish the date range, .

startYear=1970
endYear=2013
yearList=range(startYear,endYear)
monthList = range(1,13)

# begin processing
process(productsInput,productsOutput,yearList,monthList)


#createRandomIndexCSV(80,0,10e-5)
#        mu, sigma = 0, 10e-5



