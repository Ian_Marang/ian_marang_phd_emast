###################################
#Script to replace time values on 
#R_Package mon files
##################################
# import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import os
import os.path
from fnmatch import fnmatch

# container for products to fix
productList = ['alph']
              'chil',
              'etac',
              'eteq',
              'etpo',
              'gd00',
              'gd05',
              'gd10',
              'moin',
              'parr',
              'runo']

# date range
yearStart =1970
yearEnd = 2013
yearList = range(yearStart,yearEnd)
monthList = range(1,13)


 
                
# main loop to run through product list, yearlist and monthlist
for product in productList:
    for year in yearList:
        for month in monthList:
            print('Starting on %s, %s, %s')%(product,year,month)
            os.chdir('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec/e_01/1970_2012')
            for root, dirs, files in os.walk(".",topdown=True):
                for fname in files:
                    if fnmatch(fname,'*_v1m0_'+str(year)+str(month).zfill(2)+'.nc'):
                        print('opening match file %s')%(fname)
                        matchFile = Dataset(fname,'r+')
                        matchTime = matchFile.variables['time']
                        print matchTime
                        print('time to copy %s')%(matchTime[0])
            
            os.chdir('/g/data/rr9/Terrestrial_Ecosystems/Climate/eMAST/eMAST_R_Package/0_01deg/v1m1_aus/mon/land/'+product+'/e_01/1970_2012')
            for root, dirs, files in os.walk(".",topdown=True):
                for ffname in files:
                    if fnmatch(ffname,'*'+product+'_v1m1_'+str(year)+str(month).zfill(2)+'.nc'):
                        print('opening fix file %s')%(ffname)
                        print('2nd loop, time to copy %s')%(matchTime[0])
                        fixFile = Dataset(ffname,'r+')                        
                        print('fix file loaded')
                        fixFile.variables['time'][0]=matchTime[0]
                        fixTime = fixFile.variables['time']
                        print('time updated %s')%(fixTime[0])
                        fixFile.close()
                        print('Closed %s, %s, %s')%(product,year,month)
                        matchFile.close()

