# FULL SAVANNA PLOT - single file

#Import libraries
import netCDF4
from netCDF4 import Dataset
import numpy as np
import decimal
#import os, sys
#import os.path
#from osgeo import gdal, osr
#from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, cm

# Set nodata value
nodata = -9999.

# Define function for reading ncfile
def read_ncdata(infile=None, varname=None):
    """ Returns data from netcdf file as outdata for use in other functions
    Requires an infile = filename and a varname = variable """
    g = Dataset(infile,'r')
    outdata = np.array(g.variables[varname])
    outdata[outdata == nodata] = decimal.Decimal("NaN")
    return outdata;


prec_mean_gradient = read_ncdata('/g/data1/rr9/sjj576/Terrestrial_Ecosystems/Climate/eMAST/ANUClimate/0_01deg/v1m0_aus/mon/land/prec_mean/e_01/1970_2012/ANUClimate_mon_prec_mean_v1m0_aus.nc','lwe_thickness_of_precipitation_amount')

m_sav = Basemap(llcrnrlat=-26.7350,urcrnrlat=-9.0050, llcrnrlon=118.9050,urcrnrlon=153.9950,projection='mill', resolution='l')
s_lons,s_lats = m_sav.makegrid(nx,ny)
x,y=m_sav(np.flipud(s_lons),np.flipud(s_lats))
cs = m_sav.contourf(x,y,data,cmap='ocean_r')
#m_sav.drawcoastlines()
cbar = m_sav.colorbar(cs, location='bottom',pad='5%')
lab_lats = [-13.18000]
lab_lons = [130.7900]
labels = ['TERN Supersite']
x,y = m(lab_lons,lab_lats)
m_sav.plot(x,y,'wo',markersize = 5)
for label, xpt, ypt in zip(labels, x, y):
    plt.text(xpt+5000, ypt, label, fontsize=10,color='white')
plt.figure(figsize=(20,20))
#plt.show()
plt.savefig('/home/576/ijm576/plots/1970_2012_mean_full_sav.png')